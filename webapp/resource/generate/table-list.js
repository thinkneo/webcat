jQuery(function() {
    var container = jQuery(document.body).children("div.tab-component").get(0);
    new TabPanel({"container": container});
});

jQuery(function() {
    var getTableNameList = function(){
        var a = [];
        var list = document.getElementsByName("tableName");

        if(list != null) {
            if(list.length != null && list.length > 0) {
                for(var i = 0; i < list.length; i++) {
                    if(list[i].checked == true) {
                        a[a.length] = list[i].getAttribute("tableType") + ":" + list[i].value;
                    }
                }
            }
            else {
                if(list.nodeName != null && list.nodeName.toLowerCase() == "input") {
                    a[a.length] = list.getAttribute("tableType") + ":" + list.value;
                }
            }
        }
        return a;
    };

    jQuery("#tools-btn").click(function() {
        window.location.href = "/webcat/tools.html";
    });

    jQuery("#batch-btn").click(function() {
        var a = getTableNameList();

        if(a.length < 1) {
            alert("请选择项目！");
            return false;
        }

        var fileName = document.body.getAttribute("fileName");
        var connectionName = document.body.getAttribute("connectionName");
        var templateConfig = document.body.getAttribute("templateConfig");

        Ajax.request({
            "method": "post",
            "url": "/generator/batch.html",
            "data": {"fileName": fileName, "connectionName": connectionName, "templateConfig": templateConfig, "tableName": a},
            "error": function(){
                alert("系统错误，请稍后再试！");
            },
            "success": function(response){
                var json = null;

                try {
                    json = window.eval("(" + response.responseText + ")");
                }
                catch(e) {
                }

                if(json != null) {
                    if(json.message != null) {
                        alert(json.message);
                    }
                    else {
                        alert("系统错误，请稍后再试！");
                    }
                }
                else {
                    alert("系统错误，请稍后再试！");
                }
            }
        });
    });

    jQuery("#finder-btn").click(function() {
        window.location.href = "/finder/index.html";
    });

    jQuery("#export-script-btn").click(function() {
        var fileName = document.body.getAttribute("fileName");
        var connectionName = document.body.getAttribute("connectionName");

        if(Util.trim(fileName).length > 0) {
            window.location.href = "/database/script.html?fileName=" + encodeURIComponent(fileName);
            return;
        }

        if(Util.trim(connectionName).length > 0) {
            window.location.href = "/database/script.html?connectionName=" + encodeURIComponent(connectionName);
            return;
        }
    });

    jQuery("#export-data-btn").click(function() {
        var connectionName = document.body.getAttribute("connectionName");

        if(Util.trim(connectionName).length < 1) {
            alert("该连接不支持导出数据！");
            return;
        }
        var tableNameList = getTableNameList();

        if(tableNameList.length < 1) {
            alert("请选择项目！");
            return false;
        }

        var html = [];

        for(var i = 0; i < tableNameList.length; i++) {
            var tableName = tableNameList[i];
            var k = tableName.indexOf(":");

            if(k > -1) {
                tableName = tableName.substring(k + 1);
            }

            var text = document.createElement("input");
            text.name = "tableName";
            text.value = tableName;
            document.exportForm.appendChild(text);
        }

        var text = document.createElement("input");
        text.name = "connectionName";
        text.value = connectionName;
        document.exportForm.appendChild(text);

        document.exportForm.method = "get";
        document.exportForm.action = "/database/export.html";
        document.exportForm.submit();
    });
});
