(function() {
    if(typeof(com) == "undefined") {
        com = {skin: {framework: {}}};
    }

    /*
     * $RCSfile: Class.js,v $$
     * $Revision: 1.1 $
     * $Date: 2012-10-18 $
     *
     * Copyright (C) 2008 Skin, Inc. All rights reserved.
     * This software is the proprietary information of Skin, Inc.
     * Use is subject to license terms.
     */
    var Class = com.skin.framework.Class = {};

    Class.getClassId = function() {
        if(this.id == null) {
            this.id = 0;
        }

        this.id++;
        return "class_" + this.id;
    };

    /**
     * create a class
     * @param parent
     * @param constructor
     * @return Object
     */
    Class.create = function(parent, constructor, fields){
        var clazz = null;

        if(parent != null) {
            if(constructor != null) {
                clazz = function(){/* Class.create */ parent.apply(this, arguments); constructor.apply(this, arguments);};
            }
            else {
                clazz = function(){/* Class.create */ parent.apply(this, arguments);};
            }

            for(var property in parent.prototype) {
                clazz.prototype[property] = parent.prototype[property];
            }

            clazz.prototype["toString"] = parent.prototype["toString"];
            clazz.$super = parent.prototype;
        }
        else {
            if(constructor != null) {
                clazz = function(){/* Class.create */ constructor.apply(this, arguments);};
            }
            else {
                clazz = function(){/* Class.create */};
            }
            clazz.$super = {};
        }

        clazz.parent = parent;
        clazz.classId = this.getClassId();
        clazz.prototype.constructor = clazz;

        if(fields != null && fields.length > 0) {
            this.let(clazz, fields);
        }
        return clazz;
    };

    /**
     * @param instance
     * @param prototype
     * @retur Object
     */
    Class.$super = /* private */ function(instance, prototype){
        var object = {};

        for(var i in prototype) {
            if(typeof(prototype[i]) == "function") {
                object[i] = function(){prototype[i].apply(instance, arguments);};
            }
        }
        return object;
    };

    /**
     * var myduck = Class.getInstance(Animal, function(){this.swim = function(){};});
     * myduck.swim();
     * @param parent
     * @param constructor
     * @return Object
     */
    Class.getInstance = function(parent, constructor){
        return new (Class.create(parent, constructor))();
    };

    /**
     * extend properties
     * @param child
     * @param parent
     * @return Object
     */
    Class.extend = function(child, parent){
        if(child == null) {
            child = {};
        }

        for(var property in parent) {
            child[property] = parent[property];
        }
        return child;
    };

    /**
     * @param f
     * @param fields
     * @return Object
     */
    Class.let = function(f, fields){
        var p = f.prototype;

        for(var i = 0, length = fields.length; i < length; i++) {
            var name = fields[i];
            var method = name.charAt(0).toUpperCase() + name.substring(1);
            p["set" + method] = new Function(name, "this." + name + " = " + name + ";");
            p["get" + method] = new Function("return this." + name + ";");
        }
    };

    var DomUtil = {};

    DomUtil.select = function(e) {
        if(document.all) {
            /* for IE */
            var range = document.body.createTextRange();
            range.moveToElementText(e);
            range.select();
        }
        else if(window.getSelection) {
            var selection = window.getSelection();

            if(document.createRange) {
                /* for FF, Opera */
                var range = document.createRange();
                range.selectNodeContents(e);
                selection.removeAllRanges();
                selection.addRange(range);
                window.focus();
            }
            else if(selection.setBaseAndExtent) {
                /* for Safari */
                selection.setBaseAndExtent(e, 0, e, 1);
            }
            else {
            }
        }
    };

    DomUtil.getHandler = function(object, handler) {
        return function(event) {
            return handler.call(object, (event || window.event));
        }
    };

    DomUtil.addListener = function(target, type, handler) {
        if(target.addEventListener) {
            target.addEventListener(type, handler, false);
        }
        else if(target.attachEvent) {
            target.attachEvent("on" + type, handler);
        }
        else {
            target["on" + type] = handler;
        }
    };

    DomUtil.removeListener = function(target, type, handler) {
        if(target == null || type == null || handler == null) {
            return;
        }

        if(target.detachEvent) {
            target.detachEvent("on" + type, handler);
        }
        else if(target.removeEventListener) {
            target.removeEventListener(type, handler, false);
        }
        else {
            target["on" + type] = null;
        }
    };

    DomUtil.stop = function(event, returnValue) {
        if(event != null) {
            if(event.stopPropagation) {
                event.stopPropagation();
            }
            else {
                event.cancelBubble = true;
            }

            if(event.preventDefault) {
                event.preventDefault();
            }

            if(returnValue == true) {
                event.cancel = false;
                event.returnValue = true;
            }
            else {
                event.cancel = true;
                event.returnValue = false;
            }
        }
        return false;
    };

    DomUtil.getStyle = function(e, name) {
        if(e.style[name]) {
            return e.style[name];
        }
        else if(document.defaultView != null && document.defaultView.getComputedStyle != null) {
            var computedStyle = document.defaultView.getComputedStyle(e, null);

            if(computedStyle != null) {
                var property = name.replace(/([A-Z])/g, "-$1").toLowerCase();
                return computedStyle.getPropertyValue(property);
            }
        }
        else if(e.currentStyle != null) {
            return e.currentStyle[name];
        }
        return null;
    };

    DomUtil.getWidth = function(e) {
        if(e.nodeName == "BODY") {
            return document.documentElement.clientWidth;
        }

        var result = this.getStyle(e, "width");

        if(result != null) {
            result = parseInt(result.replace("px", ""));
        }
        return isNaN(result) ? 0 : result;
    };

    DomUtil.getHeight = function(e) {
        if(e.nodeName == "BODY") {
            return document.documentElement.clientWidth;
        }

        var result = this.getStyle(e, "height");

        if(result != null) {
            result = parseInt(result.replace("px", ""));
        }
        return isNaN(result) ? 0 : result;
    };

    DomUtil.show = function(id){
        try {
            var e = document.getElementById(id);

            if(e != null) {
                e.style.display = "block";
            }
        }
        catch(e){}
    };

    DomUtil.hide = function(id){
        try {
            var e = document.getElementById(id);

            if(e != null) {
                e.style.display = "none";
            }
        }
        catch(e){}
    };

    DomUtil.check = function(name, checked){
        var list = document.getElementsByName(name);

        if(list == null || list == undefined) {
            return false;
        }

        if(list.length == null || list.length == undefined) {
            list.checked = checked;
            return true;
        }

        for(var i=0; i < list.length; i++) {
            list[i].checked = checked;
        }
    };

    var Dragable = function(target) {
        this.x = 0;
        this.y = 0;

        this.target = target;
        this.frame = document.getElementById("dragable_frame");

        this.stopHandler = DomUtil.getHandler(this, this.stop);
        this.moveHandler = DomUtil.getHandler(this, this.move);

        if(this.frame == null) {
            this.frame = document.createElement("div");
            this.frame.id = "dragable_frame";

            if(document.all) {
                this.frame.innerHTML = "<div style=\"width: 100%; height: 100%; background-color: #bbccdd; filter: alpha(opacity=10); cursor: default;\"></div>";
            }
            else {
                this.frame.innerHTML = "<div style=\"width: 100%; height: 100%; background-color: #0E89E6; opacity: 0.1; cursor: default;\"></div>";
            }
            document.body.appendChild(this.frame);
        }

        if(this.target != null) {
            this.target.style.position = "absolute";
            var y = this.target.offsetTop;
            var x = this.target.offsetLeft;
            var w = this.target.offsetWidth - 0;
            var h = this.target.offsetHeight - 0;

            var cssText = "position: absolute; display: none;"
                + " width: " + w + "px; height: " + h + "px; top:" + y + "px; left:" + x + "px;"
                + " border: 1px dashed #3399FF; background-color: transparent; cursor: default; z-index: -1";
            this.frame.style.cssText = cssText;
        }
        DomUtil.addListener(this.target, "mousedown", DomUtil.getHandler(this, this.start));
    };

    Dragable.prototype.start = function(event) {
        var src = (event.srcElement || event.target);
        this.target.style.zIndex = (Dragable.zIndex++);

        if(src.getAttribute("dragable") != "true") {
            return true;
        }

        this.y = event.clientY - this.target.offsetTop;
        this.x = event.clientX - this.target.offsetLeft;

        var x = this.target.offsetLeft;
        var y = this.target.offsetTop;
        var w = this.target.offsetWidth;
        var h = this.target.offsetHeight;

        this.frame.style.top = y + "px";
        this.frame.style.left = x + "px";
        this.frame.style.width = w + "px";
        this.frame.style.height = h + "px";
        this.frame.style.display = "block";
        this.frame.style.zIndex = Dragable.zIndex + 1;

        DomUtil.addListener(document, "mouseup", this.stopHandler);
        DomUtil.addListener(document, "mousemove", this.moveHandler);
        return DomUtil.stop(event);
    };

    Dragable.prototype.move = function(event){
        var x = event.clientX - this.x;
        var y = event.clientY - this.y;
        this.frame.style.top = y + "px";
        this.frame.style.left = x + "px";
        return DomUtil.stop(event);
    };

    Dragable.prototype.stop = function(event){
        var y = this.frame.offsetTop;
        var x = this.frame.offsetLeft;

        this.target.style.marginTop = "0px";
        this.target.style.marginLeft = "0px";
        this.target.style.top = y + "px";
        this.target.style.left = x + "px";

        this.frame.style.zIndex = -1;
        this.frame.style.display = "none";

        DomUtil.removeListener(document, "mouseup", this.stopHandler);
        DomUtil.removeListener(document, "mousemove", this.moveHandler);
        return DomUtil.stop(event);
    };

    Dragable.zIndex = 1001;
    Dragable.registe = function(src) {
        var e = null;

        if(typeof(src) == "string") {
            e = document.getElementById(src);
        }
        else {
            e = src;
        }

        if(e != null) {
            if(e.style.zIndex != Dragable.zIndex) {
                Dragable.zIndex = Dragable.zIndex + 2;
                e.style.zIndex = Dragable.zIndex;
            }
            new Dragable(e);
        }
    };

    var Panel = Class.create(null, function(args) {
        var options = (args || {});

        if(typeof(options.container) == "string") {
            this.container = document.getElementById(options.container);
        }
        else {
            this.container = options.container;
        }
    });

    Panel.prototype.getContainer = function() {
        return this.container;
    };

    var TabPanel = Class.create(Panel, function() {
        this.create();
    });

    TabPanel.prototype.create = function() {
        var self = this;
        var container = this.getContainer();
        var parent = jQuery(container);
        var labelWrap = parent.children("div.tab-label-wrap");
        var panelWrap = parent.children("div.tab-panel-wrap");

        labelWrap.find("ul li.tab-label").unbind();
        labelWrap.find("ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                self.remove(this);
            }
            else {
                self.active(this);
            }
        });

        labelWrap.find("span.add").click(function() {
            if(self.add != null) {
                self.add();
            }
        });

        labelWrap.find("ul li.tab-label:eq(0)").click();

        /*
        panelWrap.children("div.tab-panel").change(function() {
            jQuery(this).find(".resize-d").each(function() {
                var offset = jQuery(this).offset();
                var clientHeight = document.documentElement.clientHeight;
                var offsetBottom = this.getAttribute("offset-bottom");

                if(offsetBottom != null) {
                    offsetBottom = parseInt(offsetBottom);
                }

                if(isNaN(offsetBottom)) {
                    offsetBottom = 0;
                }
                jQuery(this).css("height", (clientHeight - offset.top - offsetBottom) + "px");
            });
        });

        jQuery(window).bind("resize", function() {
            var panel = self.getActive();
            jQuery(panel).change();
        });
        jQuery(self.getActive()).change();
        */
    };

    TabPanel.prototype.append = function(opts) {
        var self = this;
        var label = document.createElement("li");
        var panel = document.createElement("div");
        var container = this.getContainer();

        label.className = "tab-label";
        panel.className = "tab-panel";

        if(opts.closeable == true) {
            label.innerHTML = "<span class=\"label\">" + opts.title + "</span><span class=\"close\"></span>";
        }
        else {
            label.innerHTML = "<span class=\"label\">" + opts.title + "</span>";
        }

        if(typeof(opts.content) == "string") {
            panel.innerHTML = opts.content;
        }
        else {
            panel.appendChild(opts.content);
        }

        jQuery(container).children("div.tab-label-wrap").children("ul").append(label);
        jQuery(container).children("div.tab-panel-wrap").append(panel);

        jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                self.remove(this);
            }
            else {
                self.active(this);
            }
        });

        if(opts.active != false) {
            jQuery(label).click();
        }
        return panel;
    };

    TabPanel.prototype.remove = function(ele) {
        var src = jQuery(ele);
        var index = src.index();
        var tabId = src.attr("tabId");
        var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");
        var active = src.hasClass("tab-active");

        if(active == true) {
            var size = src.parent().children("li").size();

            if((index + 1) < size) {
                src.parent().children("li:eq(" + (index + 1) + ")").click();
            }
            else if(index > 0) {
                src.parent().children("li:eq(" + (index - 1) + ")").click();
            }
            else if(size > 0) {
                src.parent().children("li:eq(" + (size - 1) + ")").click();
            }
        }

        if(tabId == null || tabId == undefined) {
            container.children("div.tab-panel:eq(" + index + ")").remove();
        }
        else {
            container.children("div.tab-panel[tabId=" + tabId + "]").remove();
        }
        src.remove();
    };

    TabPanel.prototype.active = function(ele) {
        if(typeof(ele) == "number") {
            var label = this.getLabel(ele);
            return this.active(label);
        }

        var src = jQuery(ele);
        var index = src.index();
        var tabId = src.attr("tabId");
        var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");

        src.closest("ul").find("li.tab-label").removeClass("tab-active");
        src.addClass("tab-active");

        if(tabId == null || tabId == undefined) {
            container.children("div.tab-panel").hide();
            container.children("div.tab-panel:eq(" + index + ")").show();
            container.children("div.tab-panel:eq(" + index + ")").change();
            container.children("div.tab-panel:eq(" + index + ")").trigger("active");
        }
        else {
            container.children("div.tab-panel").hide();
            container.children("div.tab-panel[tabId=" + tabId + "]").show();
            container.children("div.tab-panel[tabId=" + tabId + "]").change();
            container.children("div.tab-panel[tabId=" + tabId + "]").trigger("active");
        }
        jQuery(window).trigger("resize");
    };

    TabPanel.prototype.show = function() {
        var container = this.getContainer();
        jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label:eq(0)").click();
    };

    TabPanel.prototype.size = function() {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label").size();
    };

    TabPanel.prototype.getLabel = function(index) {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label:eq(" + index + ")").get(0);
    };

    TabPanel.prototype.getPanel = function(index) {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-panel-wrap").find("div.tab-panel:eq(" + index + ")").get(0);
    };

    TabPanel.prototype.getActive = function() {
        var container = this.getContainer();
        var index = jQuery(container).children("div.tab-label-wrap").find("ul li.tab-active").index();
        return jQuery(container).children("div.tab-panel-wrap").children("div.tab-panel:eq(" + index + ")").get(0);
    };

    var SplitPanel = Class.create(Panel, function() {
        this.left = this.getLeft();
        this.right = this.getRight();
        this.split = this.getSplit();
        this.frame = document.createElement("div");
        this.container.appendChild(this.frame);
        this.init();
    });

    SplitPanel.prototype.init = function() {
        var orientation = this.container.getAttribute("split");

        if(orientation == "x") {
            this.splitXPanel();
        }
        else {
            this.splitYPanel();
        }
    };

    SplitPanel.prototype.splitXPanel = function() {
        this.container.style.position = "relative";
        this.container.style.top = "0px";
        this.container.style.left = "0px";
        this.container.style.overflow = "hidden";

        this.setLayout(this.container);
        var parent = this.container.parentNode;
        var width = this.getWidth(this.container);
        var height = this.getHeight(this.container);
        var leftWidth = Math.floor(width / 2);
        var rightWidth = width - leftWidth - 5;

        this.left.style.float = "left";
        this.right.style.float = "left";
        this.split.style.float = "left";
        this.split.style.width = "5px";
        this.split.style.cursor = "col-resize";

        this.left.style.height = height + "px";
        this.right.style.height = height + "px";
        this.split.style.height = height + "px";

        this.left.style.width = leftWidth + "px";
        this.right.style.width = rightWidth + "px";
        this.split.style.height = height + "px";

        this.start = function(event) {
            var src = (event.srcElement || event.target);
            var keyCode = (event.keyCode || event.which);

            if(keyCode != 1) {
                return true;
            }

            this.frame.style.display = "block";
            DomUtil.addListener(document, "mouseup", this.stopHandler);
            DomUtil.addListener(document, "mousemove", this.moveHandler);
            return true;
        };

        this.move = function(event) {
            var offset = jQuery(this.container).offset();
            var x = event.clientX - offset.left - 1;
            var width = jQuery(this.container).width();

            if(x <= 0) {
                return;
            }

            this.frame.style.left = x + "px";
            return DomUtil.stop(event);
        };

        this.stop = function(event) {
            var width = jQuery(this.container).width();
            this.left.style.width = this.frame.offsetLeft + "px";
            this.right.style.width = (width - this.frame.offsetLeft - 5) + "px";
            this.frame.style.display = "none";
            this.resize();

            DomUtil.removeListener(document, "mouseup", this.stopHandler);
            DomUtil.removeListener(document, "mousemove", this.moveHandler);
            var flag = DomUtil.stop(event);

            if(this.callback != null) {
                this.callback();
            }
            return flag;
        };

        this.resize = function() {
            this.setLayout(this.container);
            var height = this.getHeight(this.container);
            this.left.style.height = height + "px";
            this.split.style.height = height + "px";
            this.right.style.height = height + "px";
        };

        var y = this.split.offsetTop;
        var x = this.split.offsetLeft;
        var cssText = "position: absolute; display: none;"
            + " width: 5px; height: " + height + "px; top: " + y + "px; left: " + x + "px;"
            + " background-color: #d8d8d8; cursor: col-resize; z-index: 10;";

        this.frame.style.cssText = cssText;
        DomUtil.removeListener(window, "resize", this.resizeHandler);
        DomUtil.removeListener(this.split, "mousedown", this.mousedownHandler);

        this.moveHandler = DomUtil.getHandler(this, this.move);
        this.stopHandler = DomUtil.getHandler(this, this.stop);
        this.mousedownHandler = DomUtil.getHandler(this, this.start);
        this.resizeHandler = DomUtil.getHandler(this, this.resize);

        // DomUtil.addListener(window, "resize", this.resizeHandler);
        DomUtil.addListener(this.split, "mousedown", this.mousedownHandler);
    };

    SplitPanel.prototype.splitYPanel = function() {
        this.container.style.position = "relative";
        this.container.style.top = "0px";
        this.container.style.left = "0px";
        this.container.style.overflow = "hidden";

        this.setLayout(this.container);
        var parent = this.container.parentNode;
        var width = this.getWidth(this.container);
        var height = this.getHeight(this.container);
        var leftHeight = Math.floor(height / 2) - 5;
        var rightHeight = (height - leftHeight);

        this.left.style.float = "none";
        this.right.style.float = "none";
        this.split.style.float = "none";
        this.split.style.height = "5px";
        this.split.style.cursor = "row-resize";
        this.left.style.height = leftHeight + "px";
        this.right.style.height = rightHeight + "px";

        this.start = function(event) {
            var src = (event.srcElement || event.target);
            var keyCode = (event.keyCode || event.which);

            if(keyCode != 1) {
                return true;
            }

            this.frame.style.display = "block";
            DomUtil.addListener(document, "mouseup", this.stopHandler);
            DomUtil.addListener(document, "mousemove", this.moveHandler);
            return true;
        };

        this.move = function(event) {
            var offset = jQuery(this.container).offset();
            var y = event.clientY - offset.top - 1;
            var height = jQuery(this.container).height();

            if(y <= 0) {
                return;
            }

            this.frame.style.top = y + "px";
            return DomUtil.stop(event);
        };

        this.stop = function(event) {
            var height = jQuery(this.container).height();
            this.left.style.height = this.frame.offsetTop + "px";
            this.right.style.height = (height - this.frame.offsetTop - 5) + "px";
            this.frame.style.display = "none";
            this.resize();

            DomUtil.removeListener(document, "mouseup", this.stopHandler);
            DomUtil.removeListener(document, "mousemove", this.moveHandler);
            var flag = DomUtil.stop(event);

            if(this.callback != null) {
                this.callback();
            }
            return flag;
        };

        this.resize = function() {
            this.setLayout(this.container);
            var height = jQuery(this.container).height();
            this.left.style.height = this.split.offsetTop + "px";
            this.right.style.height = (height - this.frame.offsetTop - 5) + "px";
            jQuery(window).trigger("resize");
        };

        var y = this.split.offsetTop;
        var x = this.split.offsetLeft;

        var cssText = "position: absolute; display: none;"
            + "width: 100%; height: 5px; top: " + y + "px; left: " + x + "px;"
            + "background-color: #d8d8d8; cursor: row-resize; z-index: 10;";

        this.frame.style.cssText = cssText;

        DomUtil.removeListener(window, "resize", this.resizeHandler);
        DomUtil.removeListener(this.split, "mousedown", this.mousedownHandler);

        this.moveHandler = DomUtil.getHandler(this, this.move);
        this.stopHandler = DomUtil.getHandler(this, this.stop);
        this.mousedownHandler = DomUtil.getHandler(this, this.start);
        this.resizeHandler = DomUtil.getHandler(this, this.resize);

        // DomUtil.addListener(window, "resize", this.resizeHandler);
        DomUtil.addListener(this.split, "mousedown", this.mousedownHandler);
    };

    SplitPanel.prototype.setLayout = function(e) {
        var layout = e.getAttribute("layout");

        if(layout == null || layout == "view") {
            var parent = e.parentNode;
            var width = this.getWidth(parent);
            var height = this.getHeight(parent);

            e.style.width = width + "px";
            e.style.height = height + "px";
        }
    };

    SplitPanel.prototype.getLeft = function() {
        if(this.left == null) {
            var list = this.getChildNodes(this.container);
            this.left = list[0];
        }
        return this.left;
    };

    SplitPanel.prototype.getRight = function() {
        if(this.right == null) {
            var list = this.getChildNodes(this.container);
            this.right = list[2];
        }
        return this.right;
    };

    SplitPanel.prototype.getSplit = function() {
        if(this.split == null) {
            var list = this.getChildNodes(this.container);
            this.split = list[1];
        }
        return this.split;
    };

    SplitPanel.prototype.getWidth = function(e) {
        if(e.nodeName == "BODY") {
            return document.documentElement.clientWidth;
        }
        else {
            return jQuery(e).width();
        }
    };

    SplitPanel.prototype.getHeight = function(e) {
        if(e.nodeName == "BODY") {
            return document.documentElement.clientHeight;
        }
        else {
            return jQuery(e).height();
        }
    };

    SplitPanel.prototype.getChildNodes = function(c) {
        var list = [];
        var childNodes = c.childNodes;

        for(var i = 0; i < childNodes.length; i++) {
            var node = childNodes[i];

            if(node.nodeType == 1) {
                list[list.length] = node;
            }
        }
        return list;
    };

    /**
     * export
     */
    window.Dragable = Dragable;
    window.TabPanel = TabPanel;
    window.SplitPanel = SplitPanel;
    window.DomUtil = DomUtil;
})();

(function() {
    DomUtil.addListener(window, "load", function() {
        DomUtil.addListener(document.body, "keydown", function(e) {
            var event = (e || window.event);
            var keyCode = event.keyCode;

            if(keyCode == 116) {
                window.location.reload();
                return DomUtil.stop(event);
            }
            return true;
        });
    });
})();

jQuery(function() {
    jQuery(window).resize(function() {
        jQuery(".resize-d").each(function() {
            var offset = jQuery(this).offset();
            var clientHeight = document.documentElement.clientHeight;
            var offsetBottom = this.getAttribute("offset-bottom");

            if(offsetBottom != null) {
                offsetBottom = parseInt(offsetBottom);
            }

            if(isNaN(offsetBottom)) {
                offsetBottom = 0;
            }

            if(this.style.overflow.length < 1) {
                if(jQuery(this).find(".resize-d").size() > 0) {
                    jQuery(this).css("overflow", "hidden");
                }
                else {
                    jQuery(this).css("overflow", "scroll");
                }
            }

            var height = clientHeight - offset.top - offsetBottom;
            jQuery(this).css("height", height + "px");
        });
    });
});

jQuery(function() {
    var getHeight = function(e) {
        if(e.nodeName == "BODY") {
            return document.documentElement.clientHeight;
        }
        else {
            return jQuery(e).height();
        }
    };

    jQuery(window).resize(function() {
        jQuery(".auto-height").each(function() {
            var clientHeight = getHeight(this.parentNode);
            jQuery(this).css("height", clientHeight + "px");
        });
    });
});

