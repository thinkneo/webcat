function exportData(){
    var connectionName = document.body.getAttribute("connectionName");

    if(jQuery.trim(connectionName).length < 1) {
        alert("该连接不支持导出数据！");
        return;
    }

    var tableNameList = getTableNameList();

    if(tableNameList.length < 1) {
        alert("请选择项目！");
        return false;
    }

    var html = [];

    for(var i = 0; i < tableNameList.length; i++) {
        var tableName = tableNameList[i];
        var k = tableName.indexOf(":");

        if(k > -1) {
            tableName = tableName.substring(k + 1);
        }

        var text = document.createElement("input");
        text.name = "tableName";
        text.value = tableName;
        document.exportForm.appendChild(text);
    }

    var text = document.createElement("input");
    text.name = "connectionName";
    text.value = connectionName;
    document.exportForm.appendChild(text);
    document.exportForm.method = "get";
    document.exportForm.action = PageContext.getContextPath() + "/database/export.html";
    document.exportForm.submit();
}

jQuery(function() {
    var tabPanel = new TabPanel({"container": "table-list-panel"});

    jQuery("div.dialog").each(function() {
        Dragable.registe(this);
    });

    jQuery("div.dialog span.close").click(function() {
        jQuery(this).closest("div.dialog").hide();
    });
});

jQuery(function() {
    var getTableNameList = function(){
        var a = [];
        var list = document.getElementsByName("tableName");

        if(list != null) {
            if(list.length != null && list.length > 0) {
                for(var i = 0; i < list.length; i++) {
                    if(list[i].checked == true) {
                        a[a.length] = list[i].getAttribute("tableType") + ":" + list[i].value;
                    }
                }
            }
            else {
                if(list.nodeName != null && list.nodeName.toLowerCase() == "input") {
                    a[a.length] = list.getAttribute("tableType") + ":" + list.value;
                }
            }
        }
        return a;
    };

    jQuery("#tools-btn").click(function() {
         window.location.href = PageContext.getContextPath() + "/webcat/tools.html";
    });

    jQuery("#batch-generate-btn").click(function() {
        var tableNameList = getTableNameList();

        if(tableNameList.length < 1) {
            alert("请选择项目！");
            return false;
        }

        var params = {};
        params.fileName = PageContext.getAttribute("fileName");
        params.connectionName = PageContext.getAttribute("connectionName");
        params.templateConfig = PageContext.getAttribute("templateConfig");
        params.tableName = tableNameList;

        jQuery.ajax({
            "type": "post",
            "url": PageContext.getContextPath() + "/generate/batch.html",
            "data": jQuery.param(params, true),
            "dataType": "json",
            "error": function(){
                alert("系统错误，请稍后再试！");
            },
            "success": function(result){
                if(result != null) {
                    if(result.message != null) {
                        alert(result.message);
                    }
                    else {
                        alert("系统错误，请稍后再试！");
                    }
                }
                else {
                    alert("系统错误，请稍后再试！");
                }
            }
        });
    });

    jQuery("#finder-btn").click(function() {
        window.open(PageContext.getContextPath() + "/finder/index.html");
    });

    jQuery("#export-sql-btn").click(function() {
        var fileName = PageContext.getAttribute("fileName");
        var connectionName = PageContext.getAttribute("connectionName");

        if(jQuery.trim(fileName).length > 0) {
            window.location.href = PageContext.getContextPath() + "/webcat/database/script.html?fileName=" + encodeURIComponent(fileName);
            return;
        }

        if(jQuery.trim(connectionName).length > 0) {
            window.location.href = PageContext.getContextPath() + "/webcat/database/script.html?connectionName=" + encodeURIComponent(connectionName);
            return;
        }
    });
});

