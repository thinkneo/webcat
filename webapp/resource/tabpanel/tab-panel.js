/*
 * $RCSfile: EventUtil.js,v $$
 * $Revision: 1.1 $
 * $Date: 2012-10-18 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
(function() {
    var EventUtil = {};

    EventUtil.getHandler = function(object, handler) {
        return function(event) {
            return handler.call(object, (event || window.event));
        }
    };

    EventUtil.addListener = function(target, type, handler) {
        if(target.addEventListener) {
            target.addEventListener(type, handler, false);
        }
        else if(target.attachEvent) {
            target.attachEvent("on" + type, handler);
        }
        else {
            target["on" + type] = handler;
        }
    };

    EventUtil.removeListener = function(target, type, handler) {
        if(target.detachEvent) {
            target.detachEvent("on" + type, handler);
        }
        else if(target.removeEventListener) {
            target.removeEventListener(type, handler, false);
        }
        else {
            target["on" + type] = null;
        }
    };

    EventUtil.stop = function(event, returnValue) {
        if(event != null) {
            if(event.stopPropagation) {
                event.stopPropagation();
            }
            else {
                event.cancelBubble = true;
            }

            if(event.preventDefault) {
                event.preventDefault();
            }

            if(returnValue == true) {
                event.cancel = false;
                event.returnValue = true;
            }
            else {
                event.cancel = true;
                event.returnValue = false;
            }
        }
        return false;
    };

    var XSplitPanel = function(container) {
        this.container = container;
        this.left = this.getLeft();
        this.right = this.getRight();
        this.split = this.getSplit();
        this.frame = document.createElement("div");
        container.appendChild(this.frame);

        container.style.position = "relative";
        container.style.top = "0px";
        container.style.left = "0px";

        if(this.left == null || this.right == null || this.split == null) {
            return;
        }

        var y = this.split.offsetTop;
        var x = this.split.offsetLeft;

        var cssText = "position: absolute; display: none;"
            + "width: 100%; height: 5px; top: " + y + "px; left: " + x + "px;"
            + "background-color: #d8d8d8; cursor: row-resize;";

        this.frame.style.cssText = cssText;
        this.moveHandler = EventUtil.getHandler(this, this.move);
        this.stopHandler = EventUtil.getHandler(this, this.stop);

        EventUtil.addListener(this.split, "mousedown", EventUtil.getHandler(this, this.start));
    };

    XSplitPanel.prototype.start = function(event) {
        var src = (event.srcElement || event.target);
        var keyCode = (event.keyCode || event.which);

        if(keyCode != 1) {
            return true;
        }

        this.frame.style.display = "block";
        EventUtil.addListener(document, "mouseup", this.stopHandler);
        EventUtil.addListener(document, "mousemove", this.moveHandler);
        return true;
    };

    XSplitPanel.prototype.move = function(event) {
        var offset = jQuery(this.container).offset();
        var y = event.clientY - offset.top - 1;
        var height = jQuery(this.container).height();

        if(y <= 0) {
            return;
        }

        this.frame.style.top = y + "px";
        return this.stopPropagation(event);
    };

    XSplitPanel.prototype.stop = function(event) {
        var height = jQuery(this.container).height();
        this.left.style.height = this.frame.offsetTop + "px";
        this.right.style.height = (height - this.frame.offsetTop - 5) + "px";
        this.frame.style.display = "none";

        EventUtil.removeListener(document, "mouseup", this.stopHandler);
        EventUtil.removeListener(document, "mousemove", this.moveHandler);

        var flag = this.stopPropagation(event);

        if(this.callback != null) {
            this.callback();
        }
        return flag;
    };

    XSplitPanel.prototype.stopPropagation = function(event){
        if(event.stopPropagation) {
            event.stopPropagation();
        }
        else {
            event.cancelBubble = true;
        }

        if(event.preventDefault) {
            event.preventDefault();
        }

        event.cancel = true;
        event.returnValue = false;
        return false;
    };

    XSplitPanel.prototype.getLeft = function() {
        if(this.left == null) {
            var list = this.getChildNodes(this.container);
            this.left = list[0];
        }
        return this.left;
    };

    XSplitPanel.prototype.getRight = function() {
        if(this.right == null) {
            var list = this.getChildNodes(this.container);
            this.right = list[2];
        }
        return this.right;
    };

    XSplitPanel.prototype.getSplit = function() {
        if(this.split == null) {
            var list = this.getChildNodes(this.container);
            this.split = list[1];
        }
        return this.split;
    };

    XSplitPanel.prototype.getChildNodes = function(c) {
        var list = [];
        var childNodes = c.childNodes;

        for(var i = 0; i < childNodes.length; i++) {
            var node = childNodes[i];

            if(node.nodeType == 1) {
                list[list.length] = node;
            }
        }
        return list;
    };
    window.XSplitPanel = XSplitPanel;
})();

(function() {
    var TabPanel = function(args) {
        var options = (args || {});

        if(typeof(options.container) == "string") {
            this.container = document.getElementById(options.container);
        }
        else {
            this.container = options.container;
        }
        this.create();
    };

    TabPanel.prototype.getContainer = function() {
        return this.container;
    };

    TabPanel.prototype.create = function() {
        var self = this;
        var container = this.getContainer();
        var parent = jQuery(container);
        var labelWrap = parent.children("div.tab-label-wrap");
        var panelWrap = parent.children("div.tab-panel-wrap");

        labelWrap.find("ul li.tab-label").unbind();
        labelWrap.find("ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                self.remove(this);
            }
            else {
                self.active(this);
            }
        });

        labelWrap.find("span.add").click(function() {
            if(self.add != null) {
                self.add();
            }
        });

        labelWrap.find("ul li.tab-label:eq(0)").click();

        panelWrap.children("div.tab-panel").change(function() {
            jQuery(this).find(".scroll-d").each(function() {
                var offset = jQuery(this).offset();
                var clientHeight = document.documentElement.clientHeight;
                var offsetBottom = this.getAttribute("offset-bottom");

                if(offsetBottom != null) {
                    offsetBottom = parseInt(offsetBottom);
                }

                if(isNaN(offsetBottom)) {
                    offsetBottom = 0;
                }
                jQuery(this).css("height", (clientHeight - offset.top - offsetBottom) + "px");
            });
        });

        jQuery(window).bind("resize", function() {
            var panel = self.getActive();
            jQuery(panel).change();
        });
        jQuery(self.getActive()).change();
    };

    TabPanel.prototype.append = function(opts) {
        var self = this;
        var label = document.createElement("li");
        var panel = document.createElement("div");
        var container = this.getContainer();

        label.className = "tab-label";
        panel.className = "tab-panel";

        if(opts.closeable == true) {
            label.innerHTML = "<span class=\"label\">" + opts.title + "</span><span class=\"close\"></span>";
        }
        else {
            label.innerHTML = "<span class=\"label\">" + opts.title + "</span>";
        }

        if(typeof(opts.content) == "string") {
            panel.innerHTML = opts.content;
        }
        else {
            panel.appendChild(opts.content);
        }

        jQuery(container).children("div.tab-label-wrap").children("ul").append(label);
        jQuery(container).children("div.tab-panel-wrap").append(panel);

        jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                self.remove(this);
            }
            else {
                self.active(this);
            }
        });

        if(opts.active != false) {
            jQuery(label).click();
        }
        return panel;
    };

    TabPanel.prototype.remove = function(ele) {
        var src = jQuery(ele);
        var index = src.index();
        var tabId = src.attr("tabId");
        var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");
        var active = src.hasClass("tab-active");

        if(active == true) {
            var size = src.parent().children("li").size();

            if((index + 1) < size) {
                src.parent().children("li:eq(" + (index + 1) + ")").click();
            }
            else if(index > 0) {
                src.parent().children("li:eq(" + (index - 1) + ")").click();
            }
            else if(size > 0) {
                src.parent().children("li:eq(" + (size - 1) + ")").click();
            }
        }

        if(tabId == null || tabId == undefined) {
            container.children("div.tab-panel:eq(" + index + ")").remove();
        }
        else {
            container.children("div.tab-panel[tabId=" + tabId + "]").remove();
        }
        src.remove();
    };

    TabPanel.prototype.active = function(ele) {
        if(typeof(ele) == "number") {
            var label = this.getLabel(ele);
            return this.active(label);
        }

        var src = jQuery(ele);
        var index = src.index();
        var tabId = src.attr("tabId");
        var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");

        src.closest("ul").find("li.tab-label").removeClass("tab-active");
        src.addClass("tab-active");

        if(tabId == null || tabId == undefined) {
            container.children("div.tab-panel").hide();
            container.children("div.tab-panel:eq(" + index + ")").show();
            container.children("div.tab-panel:eq(" + index + ")").change();
            container.children("div.tab-panel:eq(" + index + ")").trigger("active");
        }
        else {
            container.children("div.tab-panel").hide();
            container.children("div.tab-panel[tabId=" + tabId + "]").show();
            container.children("div.tab-panel[tabId=" + tabId + "]").change();
            container.children("div.tab-panel[tabId=" + tabId + "]").trigger("active");
        }
    };

    TabPanel.prototype.show = function() {
        var container = this.getContainer();
        jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label:eq(0)").click();
    };

    TabPanel.prototype.size = function() {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label").size();
    };

    TabPanel.prototype.getLabel = function(index) {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-label-wrap").find("ul li.tab-label:eq(" + index + ")").get(0);
    };

    TabPanel.prototype.getPanel = function(index) {
        var container = this.getContainer();
        return jQuery(container).children("div.tab-panel-wrap").find("div.tab-panel:eq(" + index + ")").get(0);
    };

    TabPanel.prototype.getActive = function() {
        var container = this.getContainer();
        var index = jQuery(container).children("div.tab-label-wrap").find("ul li.tab-active").index();
        return jQuery(container).children("div.tab-panel-wrap").children("div.tab-panel:eq(" + index + ")").get(0);
    };
    window.TabPanel = TabPanel;
})();

/*
var TabPanelUtil = {};

TabPanelUtil.init = function(ele) {
    if(ele != null) {
        jQuery(ele).find("div.tab-label-wrap ul li.tab-label").unbind();
        jQuery(ele).find("div.tab-label-wrap ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                TabPanelUtil.remove(this);
            }
            else {
                TabPanelUtil.active(this);
            }
        });
    }
    else {
        jQuery("div.tab-label-wrap ul li.tab-label").unbind();
        jQuery("div.tab-label-wrap ul li.tab-label").click(function(event) {
            var target = (event.target || event.srcElement);

            if(target.nodeName == "SPAN" && target.className == "close") {
                TabPanelUtil.remove(this);
            }
            else {
                TabPanelUtil.active(this);
            }
        });
    }
};

TabPanelUtil.active = function(ele) {
    var src = jQuery(ele);
    var index = src.index();
    var tabId = src.attr("tabId");
    var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");

    src.closest("ul").find("li.tab-label").removeClass("tab-active");
    src.addClass("tab-active");

    if(tabId == null || tabId == undefined) {
        container.children("div.tab-panel").hide();
        container.children("div.tab-panel:eq(" + index + ")").show();
    }
    else {
        container.children("div.tab-panel").hide();
        container.children("div.tab-panel[tabId=" + tabId + "]").show();
    }
};

TabPanelUtil.add = function(tabpanel, opts) {
    var label = document.createElement("li");
    var panel = document.createElement("div");
    label.className = "tab-label";
    panel.className = "tab-panel";

    if(opts.closeable == true) {
        label.innerHTML = "<span class=\"label\">" + opts.title + "</span><span class=\"close\"></span>";
    }
    else {
        label.innerHTML = "<span class=\"label\">" + opts.title + "</span>";
    }

    if(typeof(opts.content) == "string") {
        panel.innerHTML = opts.content;
    }
    else {
        panel.appendChild(opts.content);
    }

    jQuery(tabpanel).children("div.tab-label-wrap").children("ul").append(label);
    jQuery(tabpanel).children("div.tab-panel-wrap").append(panel);
    TabPanelUtil.init();

    if(opts.active != false) {
        jQuery(label).click();
    }
};

TabPanelUtil.remove = function(ele) {
    var src = jQuery(ele);
    var index = src.index();
    var tabId = src.attr("tabId");
    var container = src.closest("div.tab-label-wrap").siblings("div.tab-panel-wrap");
    var active = src.hasClass("tab-active");

    if(active == true) {
        var size = src.parent().children("li").size();

        if((index + 1) < size) {
            src.parent().children("li:eq(" + (index + 1) + ")").click();
        }
        else if(index > 0) {
            src.parent().children("li:eq(" + (index - 1) + ")").click();
        }
        else if(size > 0) {
            src.parent().children("li:eq(" + (size - 1) + ")").click();
        }
    }

    if(tabId == null || tabId == undefined) {
        container.children("div.tab-panel:eq(" + index + ")").remove();
    }
    else {
        container.children("div.tab-panel[tabId=" + tabId + "]").remove();
    }
    src.remove();
};

jQuery(function() {
    jQuery("div.tab-panel-wrap").each(function() {
        jQuery(this).children("div.tab-panel:eq(0)").show();
    });
    TabPanel.init();
});
*/
