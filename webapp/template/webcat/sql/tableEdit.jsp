<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Table Edit</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" href="/resource/style/default/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/tabpanel/style/tab-panel.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/ajax.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/drag.js"></script>

<script type="text/javascript" src="${contextPath}/resource/tabpanel/tab-panel.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/generate/table-edit.js"></script>
<script type="text/javascript">
//<![CDATA[
var defaultTableEditor = new TableEditor("columnTable");

defaultTableEditor.submit = function(){
    var tableDefinition = null;

    if(defaultTableEditor.validate() == true) {
        tableDefinition = this.getTableXml();
    }
    else{
        return false;
    }

    Ajax.request({
        "method": "post",
        "url": "/generator/generate.html",
        "data": {"tableDefinition": tableDefinition},
        "success": function(response){
            var json = null;

            try {
                json = window.eval("(" + response.responseText + ")");
            }
            catch(e) {
            }

            if(json != null) {
                if(json.message != null) {
                    alert(json.message);
                }
                else {
                    alert("系统错误，请稍后再试！");
                }
            }
            else {
                alert("系统错误，请稍后再试！");
            }
        }
    });
};

window.onload = function(){
    SimpleDrag.register("form_dialog_source", "form_dialog");
};
//]]>
</script>
</head>
<body style="overflow: hidden;">
<div id="table-edit-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">表结构</span></li>
            <li class="tab-label"><span class="label">索引</span></li>
            <li class="tab-label"><span class="label">脚本</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a class="button" href="/tools.html">工具</a>
                <a class="button" href="javascript:void(0)" onclick="defaultTableEditor.insert();">插入列</a>
                <a class="button" href="javascript:void(0)" onclick="defaultTableEditor.remove();">删除列</a>
            </div>
            <div class="list-panel">
                <div>
                    <table class="table" style="width: 900px;">
                        <tr class="thead">
                            <td class="c1">&nbsp;</td>
                            <td style="width: 186px;" columnIndex="1">Name</td>
                            <td style="width: 106px;" columnIndex="3">DataType</td>
                            <td style="width: 186px;" columnIndex="4">JavaName</td>
                            <td style="width: 106px;" columnIndex="5">JavaType</td>
                            <td style="width: 66px;" columnIndex="6">Length</td>
                            <td style="width: 66px;" columnIndex="7">Precision</td>
                            <td class="c1" columnIndex="8">P</td>
                            <td class="c1" columnIndex="9">M</td>
                        </tr>
                    </table>
                </div>
                <div id="columnPanel" class="scroll-d" offset-bottom="30">
                    <table id="columnTable" class="table" style="width: 900px;">
                        <c:forEach items="${columns}" var="column" varStatus="status">
                        <tr jso="${JsonUtil.stringify(column)}" title="${column.remarks}">
                            <td class="c2">${status.index + 1}</td>
                            <td style="width: 186px;"><input type="text" class="text2" style="width: 180px; height: 20px;" value="${column.columnName}"/></td>
                            <td style="width: 106px;"><input type="text" class="text2" style="width: 100px; height: 20px;" value="${column.typeName}"/></td>
                            <td style="width: 186px;"><input type="text" class="text2" style="width: 180px; height: 20px;" value="${column.variableName}"/></td>
                            <td style="width: 106px;"><input type="text" class="text2" style="width: 100px; height: 20px;" value="${column.javaTypeName}"/></td>
                            <td style="width: 66px;"><input type="text" class="text2" style="width: 60px; height: 20px;"  value="${column.columnSize}"/></td>
                            <td style="width: 66px;"><input type="text" class="text2" style="width: 60px; height: 20px;"  value="${column.decimalDigits}"/></td>
                            <td class="c2"><input type="checkbox" value="true" primaryKey="${column.primaryKey}" <c:if test="${column.primaryKey}">checked="true"</c:if>/></td>
                            <td class="c2"><input type="checkbox" value="true" nullable="${column.nullable}" <c:if test="${column.nullable == 1}">checked="true"</c:if>/></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <div class="h30"></div>
                </div>
            </div>
            <div class="status-bar">
                <div><h4 class="movetop" onclick="defaultTableEditor.moveTop()"></h4></div>
                <div><h4 class="moveup" onclick="defaultTableEditor.moveUp()"></h4></div>
                <div><h4 class="movedown" onclick="defaultTableEditor.moveDown()"></h4></div>
                <div><h4 class="movebottom" onclick="defaultTableEditor.moveBottom()"></h4></div>
            </div>
        </div>
        <div id="table-index-panel" class="tab-panel">
            <h3 style="margin: 8px;">loadding...</h3>
        </div>
        <div id="sql-script-panel" class="tab-panel">
            <pre class="sql scroll-d" offset-bottom="8"></pre>
        </div>
    </div>
</div>
<div style="display: none;">
    <form name="tableForm" method="post" onsubmit="return defaultTableEditor.validate();">
    <input type="hidden" name="alias" value="${table.alias}"/>
    <input type="hidden" name="tableType" value="${table.tableType}"/>
    <input type="hidden" name="queryName" value="${table.queryName}"/>
    <input type="hidden" name="tableCode" value="${table.tableCode}"/>
    <input type="hidden" name="tableName" value="${table.tableName}"/>
    <input type="hidden" name="className" value="${table.className}"/>
    <input type="hidden" name="remarks" value="${table.remarks}"/>
    </form>
</div>
<div id="pageContext" database="${database}"></div>
</body>
</html>
