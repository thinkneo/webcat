<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/drag.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sql-insert.js"></script>
</head>
<body style="overflow: hidden;">
<div class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">insert tool</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="panel">
                <div class="menu-bar">
                    <a class="button" href="${contextPath}/webcat/tools.html">工具箱</a>
                    <a id="parseBtn" class="button">解 析</a>
                    <a id="insertBtn" class="button">生 成</a>
                </div>
            </div>
            <div class="form-panel">
                <textarea id="source" class="editor h60" placeholder="请输入SQL语句"></textarea>
                <textarea id="result" class="editor h60" readonly="true"></textarea>
            </div>
            <div class="list-panel">
                <div>
                    <table class="table">
                        <tr class="thead">
                            <td class="w30">&nbsp;</td>
                            <td class="w200">列名</td>
                            <td class="w100">类型</td>
                            <td class="w100">可空</td>
                            <td class="w300">值</td>
                            <td class="center">操 作</td>
                        </tr>
                    </table>
                </div>
                <div id="columnPanel" style="height: 360px; padding-top: 0px; overflow-x: hidden; overflow-y: auto;">
                    <table id="columnList" class="table" style="width: 1000px;"></table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="editPanel" class="frame hide" style="position: absolute; top: 20px; left: 240px;">
    <div class="panel">
        <div class="panel-title">
            <h4 id="editPanelTitle">
                <span class="icon-table"></span>
                <span class="button close"></span>
            </h4>
        </div>
        <div class="panel-content">
            <div class="form-panel" style="width: 600px; height: 300px; overflow: auto; cursor: default;">
                <textarea name="content" class="text" style="width: 592px; height: 290px; border: 1px solid #c0c0c0; outline: none; overflow: auto;"></textarea>
            </div>
        </div>
        <div style="margin: 20px 10px 20px 0px; text-align: right;">
            <input name="ensure" type="image" src="/resource/webcat/images/ensure.gif"/>
            <input name="cancel" type="image" src="/resource/webcat/images/cancel.gif"/>
        </div>
    </div>
</div>
<script type="text/javascript">
//<![CDATA[
var table = <c:out value="${JsonUtil.stringify(table)}"/>;
//]]>
</script>
</body>
</html>