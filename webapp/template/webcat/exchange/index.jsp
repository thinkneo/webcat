<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>分类管理</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/admin/style/base.css"/>
<script type="text/javascript" src="${contextPath}/resource/admin/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
jQuery(function(){
    jQuery("#saveButton").click(function(){
        var form = jQuery("form[name=dataForm]");
        var file = form.find("input[name=file]");
        var data = form.find("textarea[name=data]");

        if(jQuery.trim(file.val()).length < 1 && jQuery.trim(data.val()).length < 1) {
            alert("请输入导入的数据！");
            return false;
        }
        form.submit();
    });
});
</script>
</head>
<body>
<div class="menu-panel"><h4>数据导入</h4></div>
<div class="menu-bar">
    <div class="button-wrap">
        <a class="button" href="javascript:void(0)" onclick="window.location.reload();"><span class="refresh">&nbsp;</span>刷新</a>
        <a id="saveButton" class="button" href="javascript:void(0)"><span class="save">&nbsp;</span>导入</a>
    </div>
</div>
<form name="dataForm" method="post" action="/webcat/exchange/import.html">
<table class="form">
    <tr>
        <td class="w80">输入文件：</td>
        <td><input name="file" type="textarea" class="w200"/></td>
    </tr>
    <tr>
        <td class="w80">输入数据：</td>
        <td><textarea name="data" class="w600 h200"/></textarea></td>
    </tr>
</table>
</form>
<div class="c20"></div>
</body>
</html>