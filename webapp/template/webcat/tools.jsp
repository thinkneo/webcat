<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>blank</title>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/tabpanel/style/tab-panel.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/browser.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
</head>
<body style="overflow: hidden;">
<div class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">工具箱</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel" style="display: block;">
            <div class="menu-bar"></div>
            <div class="outline-view" style="border: none;">
                <ul>
                    <li class="item">
                        <div class="box"><img src="${contextPath}/resource/webcat/images/xp120.gif"/></div>
                        <div class="filename"><a class="file" href="${contextPath}/webcat/tools/sql/create.html">create parse</a></div>
                    </li>
                    <li class="item">
                        <div class="box"><img src="${contextPath}/resource/webcat/images/xp120.gif"/></div>
                        <div class="filename"><a class="file" href="${contextPath}/webcat/tools/sql/insert.html">insert editor</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</body>
</html>
