<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>Webcat</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" href="/resource/style/default/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sqlplus.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/table-edit.js"></script>
</head>
<body style="overflow: hidden;">
<div id="table-edit-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">表结构</span></li>
            <li class="tab-label"><span class="label">索引</span></li>
            <li class="tab-label"><span class="label">注释</span></li>
            <li class="tab-label"><span class="label">脚本</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a id="save-column-btn" class="button" href="javascript:void(0)">保 存</a>
                <a id="insert-column-btn" class="button" href="javascript:void(0)">添加列</a>
                <a id="delete-column-btn" class="button" href="javascript:void(0)">删除列</a>
                <a id="parse-sql-btn" class="button" style="display: none;" href="javascript:void(0)">导入建表语句</a>
            </div>
            <div class="list-panel">
                <div>
                    <table class="table" style="width: 980px;">
                        <tr class="thead">
                            <td class="c1">&nbsp;</td>
                            <td style="width: 190px;" index="1">字段名</td>
                            <td style="width: 150px;" index="2">数据类型</td>
                            <td style="width: 70px;" index="3">长度</td>
                            <td style="width: 70px;" index="4">精度</td>
                            <td style="width: 70px;" index="5">默认值</td>
                            <td index="6">注释</td>
                            <td style="width: 60px;" index="7">允许空</td>
                            <td style="width: 60px;" index="8">自增</td>
                            <td style="width: 60px;" index="9">主键</td>
                        </tr>
                    </table>
                </div>
                <div class="resize-d">
                    <table id="column-table" class="table" style="width: 980px;">
                        <c:forEach items="${columns}" var="column" varStatus="status">
                        <tr jso="${JsonUtil.stringify(column)}" title="${column.remarks}">
                            <td class="c2">${status.index + 1}</td>
                            <td style="width: 190px;" index="1"><input name="columnName" type="text" class="text2" style="width: 180px;" value="${column.columnName}"/></td>
                            <td style="width: 150px;" index="2"><input name="typeName" type="text" class="text2" style="width: 140px;" value="${column.typeName}"/></td>
                            <td style="width: 70px;" index="3"><input name="columnSize" type="text" class="text2" style="width: 60px;" value="${column.columnSize}"/></td>
                            <td style="width: 70px;" index="4"><input name="decimalDigits" type="text" class="text2" style="width: 60px;" value="${column.decimalDigits}"/></td>
                            <td style="width: 70px;" index="5"><input name="columnDef" type="text" class="text2" style="width: 60px;" value="${column.columnDef}"/></td>
                            <td index="6"><input name="remarks" type="text" class="text2" style="width: 152px;" new-value="${column.remarks}" value="${column.remarks}"/><span name="remarks" class="edit"></span></td>
                            <td style="width: 60px; text-align: center; background-color: #efefef;" index="7"><input name="nullable" type="checkbox" style="vertical-align: middle;" checked-value="true" value="${column.nullable}"/></td>
                            <td style="width: 60px; text-align: center; background-color: #efefef;" index="7"><input name="autoIncrement" type="checkbox" style="vertical-align: middle;" checked-value="true" value="${column.autoIncrement}"/></td>
                            <td style="width: 60px; text-align: center; background-color: #efefef;" index="8"><input name="primaryKey" type="checkbox" style="vertical-align: middle;" checked-value="true" value="${column.primaryKey}"/></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <div class="h60"></div>
                </div>
            </div>
        </div>
        <div id="table-index-panel" class="tab-panel">
            <div class="menu-bar">
                <a id="save-index-btn" class="button" href="javascript:void(0)">保 存</a>
                <a id="insert-index-btn" class="button" href="javascript:void(0)">添加索引</a>
                <a id="delete-index-btn"class="button" href="javascript:void(0)">删除索引</a>
            </div>
            <div class="list-panel">
                <div class="resize-d">
                    <table id="index-table" class="table" style="width: 980px;">
                        <tr class="thead">
                            <td class="c1">&nbsp;</td>
                            <td style="width: 200px;" index="1">名称</td>
                            <td style="width: 300px;" index="2">字段</td>
                            <td style="width: 150px;" index="3">类型</td>
                            <td index="4">方法</td>
                        </tr>
                        <c:forEach items="${indexInfoList}" var="indexInfo" varStatus="status">
                        <tr>
                            <td class="c2">${status.index + 1}</td>
                            <td index="1"><input name="indexName" type="text" class="text2" style="width: 190px;" value="${indexInfo.indexName}"/></td>
                            <td index="2">
                                <input name="columnName" type="text" class="text2" style="width: 270px;" value="${indexInfo.columnName}"/><span class="edit"></span>
                            </td>
                            <td index="3">
                                <select name="nonUnique" style="width: 120px;" selected-value="${indexInfo.nonUnique}">
                                    <option value="1">默认</option>
                                    <option value="0">Unique</option>
                                    <option value="2">Fulltext</option>
                                    <option value="3">Spatial</option>
                                </select>
                            </td>
                            <td index="4">
                                <select name="indexType" style="width: 120px;" selected-value="${indexInfo.indexType}">
                                    <option value="">默认</option>
                                    <option value="BTREE">BTREE</option>
                                    <option value="HASH">HASH</option>
                                </select>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                    <div class="h60"></div>
                </div>
            </div>
        </div>
        <div id="comment-panel" class="tab-panel">
            <div class="menu-bar">
                <a id="save-table-btn" class="button" href="javascript:void(0)">保 存</a>
            </div>
            <div>
                <table class="form">
                    <tr>
                        <td class="w60">表名:</td>
                        <td>
                            <input type="text" name="tableName" class="required w300" maxlength="64" old-value="${table.tableName}" value="${table.tableName}"/>
                            <span class="required">*</span>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">注释:</td>
                        <td><textarea name="remarks" type="text" class="text" style="width: 480px; height: 100px;" old-value="${table.remarks}">${table.remarks}</textarea></td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="sql-script-panel" class="tab-panel">
            <div class="menu-bar">
                <a class="button select" href="javascript:void(0)">全 选</a>
            </div>
            <div>
                <pre class="sql-editor resize-d" offset-bottom="8"></pre>
            </div>
        </div>
    </div>
</div>

<div id="edit-panel" class="hide" style="padding: 0px; margin: 0px; position: absolute; top: 50px; left: 200px;">
    <textarea name="content" style="margin: 0px; padding: 4px 2px; width: 300px; height: 100px; outline: none;"></textarea>
</div>

<div id="alter-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>Alter Sql</h4>
            <span class="close"></span>
        </div>
        <div class="menu-bar">
            <a class="button select" href="javascript:void(0)">全 选</a>
        </div>
        <div class="panel-content" style="width: 800px; overflow: hidden; cursor: default;">
            <pre class="sql-editor" style="width: 790px; height: 240px;" contenteditable="true"></pre>
            <div class="operator">
                <input type="button" class="button ensure" value="执 行"/>
                <input type="button" class="button cancel" value="取 消"/>
            </div>
        </div>
    </div>
</div>

<div id="sql-create-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>导入建表语句</h4>
            <span class="close"></span>
        </div>
        <div class="menu-bar">
            <a class="button select" href="javascript:void(0)">全 选</a>
        </div>
        <div class="panel-content" style="width: 800px; overflow: hidden; cursor: default;">
            <pre class="sql-editor" style="width: 790px; height: 240px;" contenteditable="true"></pre>
            <div class="operator">
                <input type="button" class="button ensure" value="确 定"/>
                <input type="button" class="button cancel" value="取 消"/>
            </div>
        </div>
    </div>
</div>

<div id="prompt-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>请输入表名</h4>
        </div>
        <div class="menu-bar"></div>
        <div class="panel-content" style="width: 400px; overflow: hidden; cursor: default;">
            <div style="padding-top: 30px; height: 100px; border: 1px solid #c0c0c0; text-align: center;">
                <input name="input-text" type="text" style="width: 300px;" placeholder="请输入表名" value="mytable1"/>
            </div>
            <div class="operator">
                <input type="button" class="button ensure" value="确 定"/>
            </div>
        </div>
    </div>
</div>
<div id="pageContext" contextPath="${contextPath}" connectionName="${name}" database="${database}" tableName="${table.tableName}" action="${action}"></div>
</body>
</html>
