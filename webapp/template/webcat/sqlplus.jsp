<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Webcat</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sqlplus.js"></script>
</head>
<body style="overflow: hidden;">
<div id="sqlplus-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul></ul>
        <span class="add" href="javascript:void(0)"></span>
    </div>
    <div class="tab-panel-wrap"></div>
</div>
<div id="pageContext" class="hide" connectionName="${name}" database="${database}" tableName="${tableName}"></div>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>
