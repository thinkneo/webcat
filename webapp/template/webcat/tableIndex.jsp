<div class="list-panel">
    <div class="resize-d">
        <table id="index-table" class="table" style="width: 980px;">
            <tr class="thead">
                <td class="c1">&nbsp;</td>
                <td style="width: 200px;" index="1">名称</td>
                <td style="width: 300px;" index="2">字段</td>
                <td style="width: 150px;" index="3">类型</td>
                <td index="4">方法</td>
            </tr>
            <c:forEach items="${tableIndexList}" var="tableIndex" varStatus="status">
            <tr>
                <td class="c2">${status.index + 1}</td>
                <td index="1"><input name="indexName" type="text" class="text2" style="width: 190px;" value="${tableIndex.indexName}"/></td>
                <td index="2">
                    <input name="columnName" type="text" class="text2" style="width: 270px;" value="${tableIndex.columnName}"/><span class="edit"></span>
                </td>
                <td index="3">
                    <select name="nonUnique" style="width: 120px;" selected-value="${tableIndex.nonUnique}">
                        <option value="">默认</option>
                        <option value="0">Normal</option>
                        <option value="1">Unique</option>
                    </select>
                </td>
                <td index="4">
                    <select name="indexType" style="width: 120px;" selected-value="${tableIndex.indexType}">
                        <option value="">默认</option>
                        <option value="BTREE">BTREE</option>
                        <option value="HASH">HASH</option>
                        <option value="FULLTEXT">FULLTEXT</option>
                    </select>
                </td>
            </tr>
            </c:forEach>
        </table>
    </div>
</div>
