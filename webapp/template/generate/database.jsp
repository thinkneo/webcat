<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<title>Generator v1.0</title>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/htree/css/htree.css"/>
<script type="text/javascript" src="${contextPath}/resource/htree/htree.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript">
//<![CDATA[
HTree.treeNodeOnClick = function(url) {
    if(url == null || url.length < 1) {
        return;
    }
    var templateConfig = document.getElementById("templateConfig").value;

    if(templateConfig.length < 1) {
        window.top.mainFrame.location.href = url;
        return;
    }

    if(url.indexOf("?") > -1 || url.indexOf("&") > -1) {
        url = url + "&templateConfig=" + encodeURIComponent(templateConfig);
    }
    else {
        var k = url.indexOf("#");

        if(k > -1) {
            url = url.substring(0, k) + "?templateConfig=" + encodeURIComponent(templateConfig) + url.substring(k);
        }
        else {
            url = url + "?templateConfig=" + encodeURIComponent(templateConfig);
        }
    }
    window.top.mainFrame.location.href = url.replace("/webcat/sql/edit.html", "/webcat/sql/generate.html");
};

function buildTree(id, url){
    HTree.config.stylePath = "${contextPath}/resource/htree/database/";
    var tree = new HTree.TreeNode({text: "数据库", href: "javascript:void(0)"});
    tree.load(url, function(){
        this.render(document.getElementById(id));
    });
}


jQuery(function() {
    jQuery("div.resize-h").change(function() {
        var offset = jQuery(this).offset();
        var clientHeight = document.documentElement.clientHeight;
        jQuery(this).height(clientHeight - offset.top + 2);
    });

    jQuery(window).bind("resize", function() {
        jQuery("div.resize-h").change();
    });
    jQuery("div.resize-h").change();
});

jQuery(function() {
    setTimeout(function() {
        buildTree("htree", "${contextPath}/generate/database/getDatabaseXml.html");
    }, 100);
});
//]]>
</script>
</head>
<body>
<div class="left-nav">
    <div class="menu-body" style="padding-left: 8px; overflow: scroll;">
        <div style="margin: 4px 0px 4px 0px;">
            <select id="templateConfig">
                <c:forEach items="${templateConfigList}" var="templateConfig" varStatus="status">
                    <option value="${templateConfig}">${templateConfig}</option>
                </c:forEach>
            </select>
        </div>
        <div id="htree" class="htree resize-h" style="margin-top: 10px; white-space: nowrap;"></div>
    </div>
</div>
</body>
</html>
