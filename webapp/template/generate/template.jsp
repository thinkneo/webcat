<div class="menu-bar">
    <span class="bb" style="padding-left: 4px;">选择模板:</span>
    <select name="template">
    <c:forEach items="${templateConfigList}" var="current" varStatus="status">
        <c:choose>
            <c:when test="${current == templateConfig}"><option value="${current}" selected="true">${current}</option></c:when>
            <c:otherwise><option value="${current}">${current}</option></c:otherwise>
        </c:choose>
    </c:forEach>
    </select>
</div>
<div class="list-panel scroll-d" offset-bottom="0">
    <table id="templateTable" class="table">
        <tr class="thead">
            <td class="w60 center"><a href="javascript:void(0)" onclick="Util.check('templateCheckbox', this)">全 选</a></td>
            <td class="center">模 板</td>
        </tr>
        <c:set var="j" value="1"/>
        <c:forEach items="${templateList}" var="template" varStatus="status">
        <tr jso="${JsonUtil.stringify(template)}">
            <td class="w80 center" style="vertical-align: middle;">
                <img class="toggle" src="/resource/webcat/images/minus.gif"/>
                <input name="templateCheckbox" type="checkbox" style="margin: -1px 0px 0px 0px; padding: 0px; vertical-align: middle;" value="1" <c:if test="${template.enabled}">checked="true"</c:if>/>
            </td>
            <td>
                <p style="height: 26px; line-height: 26px;">模板文件：<input type="text" class="text w500" value="${template.path}"/></p>
                <p style="height: 26px; line-height: 26px;">输出路径：<input type="text" class="text w500" value="${template.output}"/></p>
            </td>
        </tr>
        <tr>
            <td class="w80 center">&nbsp;</td>
            <td class="left">
                <c:if test="${util.notEmpty(template.parameters)}">
                <table class="table" style="margin: 4px 4px 4px 0px; width: 640px; border: 1px solid #c0c0c0;">
                    <c:set var="k" value="1"/>
                    <tr>
                        <td class="w120 center gray bb">参数名</td>
                        <td class="center gray bb">参数值</td>
                    </tr>
                    <c:forEach items="${template.parameters.values()}" var="parameter" varStatus="status">
                    <tr jso="${JsonUtil.stringify(parameter)}">
                        <td class="w120 right gray bb">${parameter.name}:&nbsp;</td>
                        <td class="gray"><input type="text" name="v${j * k}" class="text w400" value="${parameter.value}"/></td>
                    </tr>
                    <c:set var="k" value="${k + 1}"/>
                    </c:forEach>
                </table>
                </c:if>
                <c:if test="${util.isEmpty(template.parameters)}">参数：无</c:if>
            </td>
        </tr>
        <c:set var="j" value="${j + 1}"/>
        </c:forEach>
    </table>
</div>
