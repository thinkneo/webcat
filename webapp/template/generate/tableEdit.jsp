<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>generator</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" href="/resource/style/default/images/favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/ajax.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/generate/table-edit.js"></script>
</head>
<body style="overflow: hidden;">
<div id="table-edit-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">表结构</span></li>
            <li class="tab-label"><span class="label">模板</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="menu-bar">
                <a class="button" href="${contextPath}/webcat/tools.html">工具</a>
                <a id="insert-column-btn" class="button" href="javascript:void(0)">插入列</a>
                <a id="remove-column-btn" class="button" href="javascript:void(0)">删除列</a>
                <a id="generate-code-btn" class="button" href="javascript:void(0)">生成代码</a>
                <a id="open-finder-btn"   class="button" href="javascript:void(0)">查看文件</a>
                <a id="create-script-btn" class="button" href="javascript:void(0)">创建脚本</a>
                <a id="insert-script-btn" class="button" href="javascript:void(0)">插入脚本</a>
                <a id="update-script-btn" class="button" href="javascript:void(0)">更新脚本</a>
            </div>
            <div class="form-panel">
                <form name="tableForm" method="post" onsubmit="return defaultTableEditor.validate();">
                <input type="hidden" name="alias" value="${table.alias}"/>
                <input type="hidden" name="tableType" value="${table.tableType}"/>
                <input type="hidden" name="queryName" value="${table.queryName}"/>
                <input type="hidden" name="tableCode" value="${table.tableCode}"/>
                <table class="form">
                    <tr>
                        <td class="w80">TableName:</td>
                        <td class="w200">
                            <input type="text" name="tableName" class="required" maxlength="64"
                                onchange="defaultTableEditor.onTableNameChange(this)" value="${table.tableName}"/>
                            <span class="required">*</span>
                        </td>
                        <td class="w80">ClassName:</td>
                        <td class="w200">
                            <input type="text" name="className" class="required" maxlength="255" value="${table.className}"/>
                            <span class="required">*</span>
                        </td>
                        <td class="w80">Remarks:</td>
                        <td><input name="remarks" type="text" class="text w300" value="${table.remarks}"/></td>
                    </tr>
                    <tr>
                        <td class="w80">Encoding:</td>
                        <td class="w200"><input type="text" name="encoding" class="text" maxlength="255" value="utf-8"/></td>
                        <td class="w80">Wrapper:</td>
                        <td class="w200"><input name="wrapper" type="checkbox" value=""/></td>
                        <td class="w80">Author:</td>
                        <td><input type="text" name="author" class="text" maxlength="255" value="${author}"/></td>
                    </tr>
                </table>
                </form>
            </div>
            <div class="list-panel">
                <div>
                    <table class="table" style="width: 900px;">
                        <tr class="thead">
                            <td class="c1">&nbsp;</td>
                            <td style="width: 186px;" columnIndex="1">Name</td>
                            <td style="width: 106px;" columnIndex="2">DataType</td>
                            <td style="width: 186px;" columnIndex="3">JavaName</td>
                            <td style="width: 106px;" columnIndex="4">JavaType</td>
                            <td style="width: 66px;" columnIndex="5">Length</td>
                            <td style="width: 66px;" columnIndex="6">Precision</td>
                            <td class="c1" columnIndex="7">P</td>
                            <td class="c1" columnIndex="8">M</td>
                        </tr>
                    </table>
                </div>
                <div id="columnPanel" class="resize-d">
                    <table id="columnTable" class="table" style="width: 900px;">
                        <c:forEach items="${columns}" var="column" varStatus="status">
                        <tr jso="${JsonUtil.stringify(column)}" title="${column.remarks}">
                            <td class="c2">${status.index + 1}</td>
                            <td style="width: 186px; height: 26px;"><input name="columnName" type="text" class="text2" style="width: 180px; height: 20px;" value="${column.columnName}"/></td>
                            <td style="width: 106px; height: 26px;"><input name="typeName" type="text" class="text2" style="width: 100px; height: 20px;" value="${column.typeName}"/></td>
                            <td style="width: 186px; height: 26px;"><input name="variableName" type="text" class="text2" style="width: 180px; height: 20px;" value="${column.variableName}"/></td>
                            <td style="width: 106px; height: 26px;"><input name="javaTypeName" type="text" class="text2" style="width: 100px; height: 20px;" value="${column.javaTypeName}"/></td>
                            <td style="width: 66px; height: 26px;"><input name="columnSize" type="text" class="text2" style="width: 60px; height: 20px;" value="${column.columnSize}"/></td>
                            <td style="width: 66px; height: 26px;"><input name="decimalDigits" type="text" class="text2" style="width: 60px; height: 20px;" value="${column.decimalDigits}"/></td>
                            <td class="c2"><input name="primaryKey" type="checkbox" checked-value="true" value="${column.primaryKey}"/></td>
                            <td class="c2"><input name="nullable" type="checkbox" checked-value="true" value="${column.nullable}"/></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <div class="h30"></div>
                </div>
            </div>
            <div class="status-bar">
                <div><h4 class="movetop" onclick="defaultTableEditor.moveTop()"></h4></div>
                <div><h4 class="moveup" onclick="defaultTableEditor.moveUp()"></h4></div>
                <div><h4 class="movedown" onclick="defaultTableEditor.moveDown()"></h4></div>
                <div><h4 class="movebottom" onclick="defaultTableEditor.moveBottom()"></h4></div>
            </div>
        </div>
        <div id="templateListPanel" class="tab-panel"></div>
    </div>
</div>
<div class="hide">
    <form name="xform" method="post" action="/generate/execute.html" onsubmit="return false;">
        <textarea name="tableDefinition" rows="10" cols="80"></textarea>
    </form>
</div>

<div id="sql-panel" class="frame hide" style="position: absolute; top: 70px; left: 20px;">
    <div class="panel">
        <div id="sql-panel-title" class="panel-title">
            <h4>创建脚本</h4>
            <span class="close"></span>
        </div>
        <div class="menu-bar">
            <div style="padding-left: 4px;">quote: <input name="quote" type="text" class="w60" value="%s"/></div>
        </div>
        <div class="panel-content">
            <pre class="sql-editor" style="width: 596px; height: 246px;"></pre>
            <div class="operator">
                <input type="button" class="button ensure" value="关 闭"/>
            </div>
        </div>
    </div>
</div>
<div id="pageContext" contextPath="${contextPath}" connectionName="${name}" database="${database}" tableName="${table.tableName}" templateConfig="${templateConfig}"></div>
</body>
</html>
