<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>管理控制台</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/frame.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/frame.js"></script>
</head>
<body contextPath="${contextPath}">
<div class="apptop">
    <div class="logo"></div>
    <div style="margin-left: 300px; height: 48px;">
        <div style="height: 30px;">
            <div class="tools-menu">
                <ul>
                    <li class="icon-01" onclick="App.refresh()">刷新</li>
                    <c:if test="${notexists}"><li class="icon-07" onclick="App.setting(event);">设置</li></c:if>
                    <li class="icon-08" onclick="App.help()">帮助</li>
                    <li class="icon-09" onclick="App.logout()">退出</li>
                </ul>
            </div>
        </div>
        <div id="channel_menu" class="channel-menu"><span style="color: #f5faff;">Hello World</span></div>
    </div>
</div>
<div class="module-group">
    <div id="module_group_menu" class="module-group-menu">
        <div class="module-group-menu-bar">
            <ul>
                <li class="clicked" title="数据库管理" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">数据库管理</li>
                <li title="代码生成" viewtype="1" lefturl="${contextPath}/generate/database.html" viewurl="${contextPath}/webcat/blank.html">代码生成</li>
                <li title="编程文档" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">编程文档</li>
                <li title="我的笔记" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">我的笔记</li>
                <!-- li title="工作周报" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">工作周报</li -->
                <li title="文件管理" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">文件管理</li>
                <li title="系统设置" viewtype="1" lefturl="${contextPath}/webcat/database.html" viewurl="${contextPath}/webcat/blank.html">系统设置</li>
            </ul>
        </div>
    </div>
</div>
<div id="viewPanel" mainPanel="view">
    <div style="float: left; width: 10px;">
        <div id="leftPanel" class="left-panel"><iframe id="leftFrame" name="leftFrame" class="left-frame"
            src="about:blank" frameborder="0" scrolling="no" marginwidth="0" marginheight="0"></iframe></div>
        <div id="ctrlPanel" class="ctrl-panel"><img id="ctrlBtn" style="border: none; cursor: default;" src="${contextPath}/resource/webcat/images/gt.gif"/></div>
    </div>
    <div id="mainPanel" class="main-panel"><iframe id="mainFrame" name="mainFrame" class="main-frame"
        src="${contextPath}/webcat/blank.html" frameborder="0" scrolling="auto" marginwidth="0" marginheight="0"></iframe></div>
</div>
<div id="statusBar" class="status-bar hide">
    <div id="_task_bar" class="widget-task-bar"></div>
    <div style="float: right;">
        <img style="margin-top: -2px; margin-right: 4px;" src="${contextPath}/resource/webcat/images/sound.gif"/>欢迎您，来自<span style="color: #9c3000;">[127.0.0.1]</span>的<span style="color: #9c3000;">admin</span>，您上次的登录时间是<span style="color: #9c3000;">[2016-11-05 20:39:06]</span>，祝您工作愉快！
    </div>
</div>
</body>
</html>