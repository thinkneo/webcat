<t:include file="/include/common.jsp"/>
/*
 * $RCSfile: ${managerClassName}.java,v $$
 * $Revision: 1.1 $
 * $Date: ${timestamp} $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package ${managerPackageName};

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.skin.core.cache.CacheClient;
import ${daoItfcPackageName}.${daoItfcClassName};
import ${modelPackageName}.${modelClassName};

/**
 * <p>Title: ${managerClassName}</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author ${author}
 * @version 1.0
 */
@Component
public class ${managerClassName} {
    @Resource
    private ${daoItfcClassName} ${daoImplVariableName};

    /**
     * @param bookId
     * @return ${modelClassName}
     */
    public ${modelClassName} getById(Long bookId) {
        ${modelClassName} ${modelVariableName} = this.${daoImplVariableName}.get(bookId);
        return ${modelVariableName};
    }
}
