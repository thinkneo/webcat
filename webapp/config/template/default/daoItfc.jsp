<t:include file="/include/common.jsp"/>
/*
 * $RCSfile: ${daoItfcClassName}.java,v $$
 * $Revision: 1.1 $
 * $Date: ${timestamp} $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package ${daoItfcPackageName};

import com.skin.j2ee.dao.Dao;
import ${modelPackageName}.${modelClassName};

/**
 * <p>Title: ${daoItfcClassName}</p>
 * <p>Description: ${variableContext.getString("Description")}</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author ${author}
 * @version 1.0
 */
public interface ${daoItfcClassName} extends Dao<${modelClassName}, ${primaryKeyJavaTypeWrapName}> {
}
