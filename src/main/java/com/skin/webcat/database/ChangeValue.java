/*
 * $RCSfile: ChangeValue.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-01 $
 *
 * Copyright (C) 2005 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database;

/**
 * <p>Title: ChangeValue</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ChangeValue {
    private String oldValue;
    private String newValue;

    /**
     * apply
     */
    public void apply() {
        this.oldValue = this.newValue;
    }

    /**
     * @return boolean
     */
    public boolean modified() {
        if(this.oldValue != null) {
            return !this.oldValue.equals(this.newValue);
        }

        if(this.newValue != null) {
            return !this.newValue.equals(this.oldValue);
        }
        return false;
    }

    /**
     * @param value
     * @return boolean
     */
    public boolean equals(String value) {
        if(this.newValue == null) {
            return (value == null);
        }
        else {
            return this.newValue.equals(value);
        }
    }

    /**
     * @return boolean
     */
    public boolean isEmpty() {
        return this.isEmpty(this.newValue);
    }

    /**
     * @param value
     * @return boolean
     */
    public boolean isEmpty(String value) {
        return (value == null || value.toString().trim().length() < 1);
    }

    /**
     * @return boolean
     */
    public boolean notEmpty() {
        return !this.isEmpty();
    }

    /**
     * @param value
     * @return boolean
     */
    public boolean notEmpty(String value) {
        return !this.isEmpty(value);
    }

    /**
     * @return boolean
     */
    public boolean getBoolean() {
        return this.getBoolean(this.newValue);
    }

    /**
     * @param value
     * @return boolean
     */
    public boolean getBoolean(String value) {
        return (value != null && value.equals("true"));
    }

    /**
     * @return int
     */
    public int getInt() {
        if(this.newValue == null) {
            throw new NullPointerException("new value is null !");
        }

        try {
            return Integer.parseInt(this.newValue);
        }
        catch(NumberFormatException e) {
        }
        return 0;
    }
    
    /**
     * @return String
     */
    public String getString() {
        return (this.newValue != null ? this.newValue : "");
    }

    /**
     * @return the oldValue
     */
    public String getOldValue() {
        return this.oldValue;
    }
    
    /**
     * @param oldValue the oldValue to set
     */
    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }
    
    /**
     * @return the newValue
     */
    public String getNewValue() {
        return this.newValue;
    }

    /**
     * @param newValue the newValue to set
     */
    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    /**
     * @param oldValue
     * @param newValue
     */
    public void set(String oldValue, String newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    /**
     * @return ChangeValue
     */
    @Override
    public ChangeValue clone() {
        ChangeValue changeValue = new ChangeValue();
        changeValue.setOldValue(this.oldValue);
        changeValue.setNewValue(this.newValue);
        return changeValue;
    }
}
