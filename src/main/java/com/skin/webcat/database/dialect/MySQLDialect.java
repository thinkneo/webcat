/*
 * $RCSfile: MySQLDialect.java,v $
 * $Revision: 1.1 $
 * $Date: 2009-3-22 $
 *
 * Copyright (C) 2005 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.dialect;

import java.sql.Types;

import com.skin.webcat.database.Column;

/**
 * <p>Title: MySQLDialect</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class MySQLDialect implements Dialect {
    /**
     * @param column
     */
    public void process(Column column) {
        int dataType = column.getDataType();
        String typeName = column.getTypeName();
        String javaTypeName = this.convert(column);

        if(typeName.endsWith("UNSIGNED")) {
            typeName = typeName.substring(0, typeName.length() - 8);
            column.setTypeName(typeName);
            column.setUnsigned(true);
        }

        column.setJavaTypeName(javaTypeName);

        if(dataType == Types.DATE || dataType == Types.TIME || dataType == Types.DATALINK || dataType == Types.TIMESTAMP) {
            column.setColumnSize(0);
            column.setDecimalDigits(0);
        }

        if(dataType == Types.LONGVARCHAR || dataType == Types.LONGVARBINARY || dataType == Types.LONGVARCHAR) {
            column.setColumnSize(0);
            column.setDecimalDigits(0);
        }
    }

    /**
     * @param column
     * @return String
     */
    @Override
    public String convert(Column column) {
        String result = "String";
        String typeName = column.getTypeName().toUpperCase();

        if("CHAR".equals(typeName)) {
            /**
             * String
             */
        }
        else if("CHARACTER".equals(typeName)) {
            /**
             * String
             */
        }
        else if("VARCHAR".equals(typeName)) {
            /**
             * String
             */
        }
        else if("VARCHAR2".equals(typeName)) {
            /**
             * String
             */
        }
        else if("TEXT".equals(typeName)) {
            /**
             * String
             */
        }
        else if("LONGTEXT".equals(typeName)) {
            /**
             * String
             */
        }
        else if("ENUM".equals(typeName)) {
            /**
             * String
             */
        }
        else if("BOOL".equals(typeName)
                || "BOOLEAN".equals(typeName)) {
            /**
             * These types are synonyms for TINYINT(1). A value of zero is considered false. Nonzero values are considered true: 
             */
            result = "boolean";
        }
        else if("BIT".equals(typeName)) {
            /**
             * A bit-field type. M indicates the number of bits per value, from 1 to 64. The default is 1 if M is omitted.
             * This data type was added in MySQL 5.0.3 for MyISAM, and extended in 5.0.5 to MEMORY, InnoDB, BDB, and NDBCLUSTER. Before 5.0.3, BIT is a
             * synonym for TINYINT(1).
             */
            result = "int";
        }
        else if("TINYINT".equals(typeName) || "TINYINT UNSIGNED".equals(typeName)) {
            /**
             * A very small integer. The signed range is -128 to 127.
             * The unsigned range is 0 to 255. 
             */
            result = "int";
        }
        else if("SMALLINT".equals(typeName) || "SMALLINT UNSIGNED".equals(typeName)) {
            /**
             * A small integer. The signed range is -32768 to 32767.
             * The unsigned range is 0 to 65535.
             */
            result = "int";
        }
        else if("MEDIUMINT".equals(typeName) || "MEDIUMINT UNSIGNED".equals(typeName)) {
            /**
             * A medium-sized integer. The signed range is -8388608 to 8388607.
             * The unsigned range is 0 to 16777215.
             */
            result = "int";
        }
        else if("INT".equals(typeName) || "INT UNSIGNED".equals(typeName)) {
            /**
             * A normal-size integer. The signed range is -2147483648 to 2147483647.
             * The unsigned range is 0 to 4294967295.
             * Integer.MAX_VALUE: -2147483648
             * Integer.MAX_VALUE: +2147483647
             */
            // Integer.MAX_VALUE: -2147483648
            // Integer.MAX_VALUE: +2147483647
            if(column.getColumnSize() > 10) {
                result = "long";
            }
            else {
                result = "int";
            }
        }
        else if("BIGINT".equals(typeName) || typeName.equals("BIGINT UNSIGNED")) {
            /**
             * A large integer. The signed range is -9223372036854775808 to 9223372036854775807.
             * The unsigned range is 0 to 18446744073709551615.
             */
            result = "long";
        }
        else if("INTEGER".equals(typeName)) {
            /**
             * This type is a synonym for INT. 
             * Integer.MAX_VALUE: -2147483648
             * Integer.MAX_VALUE: +2147483647
             */
            if(column.getColumnSize() >= 10) {
                result = "long";
            }
            else {
                result = "int";
            }
        }
        else if("FLOAT".equals(typeName)) {
            result = "float";
        }
        else if("DOUBLE".equals(typeName)) {
            result = "double";
        }
        else if("LONG".equals(typeName)) {
            result = "long";
        }
        else if("NUMBER".equals(typeName)) {
            if(column.getDecimalDigits() > 0) {
                result = "double";
            }
            else {
                // Integer.MAX_VALUE: -2147483648
                // Integer.MAX_VALUE: +2147483647
                if(column.getColumnSize() >= 10) {
                    result = "long";
                }
                else {
                    result = "int";
                }
            }
        }
        else if("DATE".equals(typeName) || "TIME".equals(typeName)) {
            result = "java.util.Date";
        }
        else if("DATETIME".equals(typeName)) {
            result = "java.util.Date";
        }
        else if("TIMESTAMP".equals(typeName)) {
            result = "java.util.Date";
        }
        else if("YEAR".equals(typeName)) {
            result = "int";
        }
        else if("BLOB".equals(typeName)) {
            result = "byte[]";
            result = "java.io.InputStream";
        }
        else if("CLOB".equals(typeName)) {
            result = "String";
        }
        else if("RAW".equals(typeName)) {
            result = "java.math.BigDecimal";
        }
        else {
            System.out.println("Warnning: Unknown DataType: " + column.getTableName() + "." + column.getColumnName() + ": " + typeName);
        }
        return result;
    }

    /**
     * @param tableName
     * @return String
     */
    @Override
    public String getTableName(String tableName) {
        return tableName;
    }

    /**
     * @param columnName
     * @return String
     */
    @Override
    public String getColumnName(String columnName) {
        return columnName;
    }

    /**
     * @param source
     * @return String
     */
    @Override
    public String escape(String source) {
        if(source == null) {
            return "";
        }

        char c;
        StringBuilder buffer = new StringBuilder();

        for(int i = 0, length = source.length(); i < length; i++) {
            c = source.charAt(i);

            switch (c) {
                case '\'': {
                    buffer.append("\\'");break;
                }
                case '\r': {
                    buffer.append("\\r");break;
                }
                case '\n': {
                    buffer.append("\\n");break;
                }
                case '\t': {
                    buffer.append("\\t");break;
                }
                case '\b': {
                    buffer.append("\\b");break;
                }
                case '\f': {
                    buffer.append("\\f");break;
                }
                case '\\': {
                    buffer.append("\\\\");break;
                }
                default : {
                    buffer.append(c);break;
                }
            }
        }
        return buffer.toString();
    }
}
