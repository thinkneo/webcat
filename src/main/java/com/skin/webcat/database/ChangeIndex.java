/*
 * $RCSfile: ChangeColumn.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-01 $
 *
 * Copyright (C) 2005 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database;

import java.util.List;

/**
 * <p>Title: ChangeColumn</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ChangeIndex extends Changeable {
    private ChangeValue indexName;
    private ChangeValue columnName;
    private ChangeValue nonUnique;
    private ChangeValue indexType;

    /**
     * @return the indexName
     */
    public ChangeValue getIndexName() {
        return this.indexName;
    }

    /**
     * @param indexName the indexName to set
     */
    public void setIndexName(ChangeValue indexName) {
        this.indexName = indexName;
    }

    /**
     * @return the columnName
     */
    public ChangeValue getColumnName() {
        return this.columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(ChangeValue columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the nonUnique
     */
    public ChangeValue getNonUnique() {
        return this.nonUnique;
    }

    /**
     * @param nonUnique the nonUnique to set
     */
    public void setNonUnique(ChangeValue nonUnique) {
        this.nonUnique = nonUnique;
    }

    /**
     * @return the indexType
     */
    public ChangeValue getIndexType() {
        return this.indexType;
    }

    /**
     * @param indexType the indexType to set
     */
    public void setIndexType(ChangeValue indexType) {
        this.indexType = indexType;
    }

    /**
     * @return boolean
     */
    @Override
    public boolean modified() {
        return (this.indexName.modified()
                || this.columnName.modified()
                || this.nonUnique.modified()
                || this.indexType.modified());
    }

    /**
     * @return boolean
     */
    @Override
    public String validate() {
        /**
         * 对所有客户端传过来拼接sql的参数做校验
         */
        if(!this.validate(this.indexName)) {
            return "bad index name: [" + this.indexName.getOldValue() + "] - " + "[" + this.indexName.getNewValue() + "]";
        }

        if(!this.validate(this.columnName.getOldValue(), ",")) {
            return "bad column name: [" + this.columnName.getOldValue() + "] - " + "[" + this.columnName.getNewValue() + "]";
        }

        if(!this.validate(this.columnName.getNewValue(), ",")) {
            return "bad column name: [" + this.columnName.getOldValue() + "] - " + "[" + this.columnName.getNewValue() + "]";
        }

        if(!this.validate(this.indexType)) {
            return "bad index type: [" + this.indexType.getOldValue() + "] - " + "[" + this.indexType.getNewValue() + "]";
        }
        return null;
    }

    /**
     * @param tableName
     * @return String
     */
    @Override
    public String getCreateSql(String tableName) {
        return this.getAlterSql(tableName);
    }

    /**
     * @param tableName
     * @return String
     */
    @Override
    public String getAlterSql(String tableName) {
        return this.getAlterSql(tableName, false);
    }

    /**
     * @param tableName
     * @param force
     * @return String
     */
    public String getAlterSql(String tableName, boolean force) {
        if(!force && !this.modified()) {
            return "";
        }

        StringBuilder buffer = new StringBuilder();

        /**
         * ALTER TABLE 表名 ADD [UNIQUE | FULLTEXT | SPATIAL] INDEX 索引名(字段名 [(长度)] [ASC | DESC]);
         */
        buffer.append("alter table ");
        buffer.append(tableName);
        buffer.append(" add");

        if(this.nonUnique.equals("0")) {
            buffer.append(" UNIQUE");
        }
        else if(this.nonUnique.equals("1")) {
        }
        else if(this.nonUnique.equals("2")) {
            buffer.append(" FULLTEXT");
            this.indexType.setNewValue("");
        }
        else if(this.nonUnique.equals("3")) {
            buffer.append(" SPATIAL");
            this.indexType.setNewValue("");
        }

        buffer.append(" index ");
        buffer.append(this.indexName.getString());
        buffer.append(" (");
        buffer.append(this.columnName.getString());
        buffer.append(")");

        if(this.indexType.notEmpty()) {
            buffer.append(" using ");
            buffer.append(this.indexType.getString());
        }
        buffer.append(";");
        return buffer.toString();
    }

    /**
     * @param tableName
     * @param changeList
     * @return String
     */
    public static String getCreateSql(String tableName, List<ChangeIndex> changeList) {
        StringBuilder buffer = new StringBuilder();

        for(ChangeIndex changeIndex : changeList) {
            String sql = changeIndex.getAlterSql(tableName, true);

            if(sql != null && sql.length() > 0) {
                buffer.append(sql);
                buffer.append("\r\n");
            }
        }
        return buffer.toString();
    }

    /**
     * @param tableName
     * @param changeList
     * @return String
     */
    public static String getAlterSql(String tableName, List<ChangeIndex> changeList) {
        StringBuilder buffer = new StringBuilder();

        for(ChangeIndex changeIndex : changeList) {
            ChangeValue indexName = changeIndex.getIndexName();

            if(!changeIndex.modified() || indexName.isEmpty()) {
                continue;
            }

            String sql = changeIndex.getAlterSql(tableName);

            if(sql != null && sql.length() > 0) {
                buffer.append(sql);
                buffer.append("\r\n");
            }
        }
        return buffer.toString();
    }

    /**
     * @param tableName
     * @param changeList
     * @return String
     */
    public static String getRemoveSql(String tableName, List<ChangeIndex> changeList) {
        StringBuilder buffer = new StringBuilder();

        for(ChangeIndex changeIndex : changeList) {
            ChangeValue indexName = changeIndex.getIndexName();

            /**
             * 如果改索引没有改动过或者是新增的索引那么跳过
             */
            if(!changeIndex.modified() || indexName.isEmpty(indexName.getOldValue())) {
                continue;
            }

            buffer.append("alter table ");
            buffer.append(tableName);
            buffer.append(" drop index ");
            buffer.append(indexName.getOldValue());
            buffer.append(";\r\n");
        }
        return buffer.toString();
    }
}
