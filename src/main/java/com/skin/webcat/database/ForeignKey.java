/*
 * $RCSfile: ForeignKey.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-03-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database;

/**
 *  1. PKTABLE_CAT String => primary key table catalog being imported (may be null)
 *  2. PKTABLE_SCHEM String => primary key table schema being imported (may be null)
 *  3. PKTABLE_NAME String => primary key table name being imported
 *  4. PKCOLUMN_NAME String => primary key column name being imported
 *  5. FKTABLE_CAT String => foreign key table catalog (may be null)
 *  6. FKTABLE_SCHEM String => foreign key table schema (may be null)
 *  7. FKTABLE_NAME String => foreign key table name
 *  8. FKCOLUMN_NAME String => foreign key column name
 *  9. KEY_SEQ short => sequence number within a foreign key( a value of 1 represents the first column of the foreign key, a value of 2 would represent the second column within the foreign key).
 * 10. UPDATE_RULE short => What happens to a foreign key when the primary key is updated:
 *         importedNoAction - do not allow update of primary key if it has been imported
 *         importedKeyCascade - change imported key to agree with primary key update
 *         importedKeySetNull - change imported key to NULL if its primary key has been updated
 *         importedKeySetDefault - change imported key to default values if its primary key has been updated
 *         importedKeyRestrict - same as importedKeyNoAction (for ODBC 2.x compatibility)
 * 11. DELETE_RULE short => What happens to the foreign key when primary is deleted.
 *         importedKeyNoAction - do not allow delete of primary key if it has been imported
 *         importedKeyCascade - delete rows that import a deleted key
 *         importedKeySetNull - change imported key to NULL if its primary key has been deleted
 *         importedKeyRestrict - same as importedKeyNoAction (for ODBC 2.x compatibility)
 *         importedKeySetDefault - change imported key to default if its primary key has been deleted
 * 12. FK_NAME String => foreign key name (may be null)
 * 13. PK_NAME String => primary key name (may be null)
 * 14. DEFERRABILITY short => can the evaluation of foreign key constraints be deferred until commit
 *         importedKeyInitiallyDeferred - see SQL92 for definition
 *         importedKeyInitiallyImmediate - see SQL92 for definition
 *         importedKeyNotDeferrable - see SQL92 for definition
 * <p>Title: ForeignKey</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ForeignKey {

}
