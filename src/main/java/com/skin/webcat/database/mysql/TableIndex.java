/*
 * $RCSfile: TableIndex.java,v $$
 * $Revision: 1.1 $
 * $Date: 2016-11-5 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.mysql;

/**
 * <p>Title: TableIndex</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableIndex {
    private String table;
    private int nonUnique;
    private String indexName;
    private int seqInIndex;
    private String columnName;
    private String collation;
    private long cardinality;
    private int subPart;
    private String packed;
    private String nullable;
    private String indexType;
    private String comment;

    /**
     * @return the table
     */
    public String getTable() {
        return this.table;
    }

    /**
     * @param table the table to set
     */
    public void setTable(String table) {
        this.table = table;
    }

    /**
     * @return the nonUnique
     */
    public int getNonUnique() {
        return this.nonUnique;
    }

    /**
     * @param nonUnique the nonUnique to set
     */
    public void setNonUnique(int nonUnique) {
        this.nonUnique = nonUnique;
    }

    /**
     * @return the indexName
     */
    public String getIndexName() {
        return this.indexName;
    }

    /**
     * @param indexName the indexName to set
     */
    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    /**
     * @return the seqInIndex
     */
    public int getSeqInIndex() {
        return this.seqInIndex;
    }

    /**
     * @param seqInIndex the seqInIndex to set
     */
    public void setSeqInIndex(int seqInIndex) {
        this.seqInIndex = seqInIndex;
    }

    /**
     * @return the columnName
     */
    public String getColumnName() {
        return this.columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the collation
     */
    public String getCollation() {
        return this.collation;
    }

    /**
     * @param collation the collation to set
     */
    public void setCollation(String collation) {
        this.collation = collation;
    }

    /**
     * @return the cardinality
     */
    public long getCardinality() {
        return this.cardinality;
    }

    /**
     * @param cardinality the cardinality to set
     */
    public void setCardinality(long cardinality) {
        this.cardinality = cardinality;
    }

    /**
     * @return the subPart
     */
    public int getSubPart() {
        return this.subPart;
    }

    /**
     * @param subPart the subPart to set
     */
    public void setSubPart(int subPart) {
        this.subPart = subPart;
    }

    /**
     * @return the packed
     */
    public String getPacked() {
        return this.packed;
    }

    /**
     * @param packed the packed to set
     */
    public void setPacked(String packed) {
        this.packed = packed;
    }

    /**
     * @return the nullable
     */
    public String getNullable() {
        return this.nullable;
    }

    /**
     * @param nullable the nullable to set
     */
    public void setNullable(String nullable) {
        this.nullable = nullable;
    }

    /**
     * @return the indexType
     */
    public String getIndexType() {
        return this.indexType;
    }

    /**
     * @param indexType the indexType to set
     */
    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return this.comment;
    }

    /**
     * @param comment the comment to set
     */
    public void setComment(String comment) {
        this.comment = comment;
    }
}

