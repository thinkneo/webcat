/*
 * $RCSfile: Record.java,v $$
 * $Revision: 1.1 $
 * $Date: 2016-3-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.sql;

import java.util.List;

/**
 * <p>Title: Record</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Record {
    private List<Object> values;

    /**
     * 
     */
    public Record() {
    }

    /**
     * @param values
     */
    public Record(List<Object> values) {
        this.values = values;
    }

    /**
     * @return the values
     */
    public List<Object> getValues() {
        return this.values;
    }

    /**
     * @param values the values to set
     */
    public void setValues(List<Object> values) {
        this.values = values;
    }
}
