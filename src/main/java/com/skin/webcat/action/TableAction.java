/*
 * $RCSfile: TableAction.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.database.Column;
import com.skin.webcat.database.IndexInfo;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.dialect.Dialect;
import com.skin.webcat.database.dialect.MySQLDialect;
import com.skin.webcat.database.handler.TableHandler;
import com.skin.webcat.database.mysql.MySql;
import com.skin.webcat.database.sql.parser.CreateParser;
import com.skin.webcat.util.Jdbc;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: TableAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableAction extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(TableAction.class);

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/table/list.html")
    public void list() throws ServletException, IOException {
        Connection connection = null;
        String name = this.getTrimString("name", "");
        String database = this.getTrimString("database");
        String type = this.getTrimString("type");

        if(type.equalsIgnoreCase("table") || type.equalsIgnoreCase("view")) {
            type = type.toUpperCase();
        }
        else {
            type = "TABLE";
        }

        try {
            connection = Webcat.getConnection(name, database);
            TableHandler tableHandler = new TableHandler(connection);
            List<Table> tableList = tableHandler.getTableList(null, null, "%", new String[]{type}, false);
            this.setAttribute("type", type);
            this.setAttribute("name", name);
            this.setAttribute("database", database);
            this.setAttribute("tableList", tableList);
        }
        catch(SQLException e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            Jdbc.close(connection);
        }
        this.forward("/template/webcat/tableList.jsp");
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/table/create.html")
    public void create() throws ServletException, IOException {
        String name = this.getTrimString("name", "");
        String database = this.getTrimString("database");

        this.setAttribute("name", name);
        this.setAttribute("database", database);
        this.setAttribute("action", "create");
        this.forward("/template/webcat/tableEdit.jsp");
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/table/edit.html")
    public void edit() throws ServletException, IOException {
        this.execute("/template/webcat/tableEdit.jsp");
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/table/parse.html")
    public void parse() throws ServletException, IOException {
        String sql = this.getTrimString("sql", "");

        try {
            Dialect dialect = new MySQLDialect();
            CreateParser parser = new CreateParser(dialect);
            List<Table> tableList = parser.parse(sql);
            Table table = null;
    
            if(tableList.size() > 0) {
                table = tableList.get(0);
            }

            if(table != null) {
                JsonUtil.success(this.request, this.response, table);    
            }
            else {
                JsonUtil.error(this.request, this.response, "解析失败！");
            }
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
            JsonUtil.error(this.request, this.response, e.getMessage());
        }
    }

    /**
     * @throws ServletException
     * @throws IOException
     */
    @UrlPattern("/webcat/table/getTableIndex.html")
    public void getTableIndex() throws ServletException, IOException {
        Connection connection = null;
        String name = this.getTrimString("name", "");
        String database = this.getTrimString("database");
        String tableName = this.getTrimString("tableName");

        try {
            connection = Webcat.getConnection(name, database);
            List<IndexInfo> indexInfoList = MySql.getIndexInfoList(connection, tableName, false, true);

            this.setAttribute("name", name);
            this.setAttribute("database", database);
            this.setAttribute("tableName", tableName);
            this.setAttribute("indexInfoList", indexInfoList);
        }
        catch(SQLException e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            Jdbc.close(connection);
        }
        this.forward("/template/webcat/tableIndex.jsp");
    }

    /**
     * @param page
     * @throws ServletException
     * @throws IOException
     */
    public void execute(String page) throws ServletException, IOException {
        Connection connection = null;
        String name = this.getTrimString("name", "");
        String database = this.getTrimString("database");
        String tableName = this.request.getParameter("tableName");

        try {
            connection = Webcat.getConnection(name, database);
            TableHandler tableHandler = new TableHandler(connection);
            Table table = tableHandler.getTable(tableName);
            List<Column> columns = table.getColumns();
            List<IndexInfo> indexInfoList = MySql.getIndexInfoList(connection, tableName, false, true);

            if(columns != null) {
                for(Column column : columns) {
                    column.setTable(null);
                }
            }

            this.setAttribute("name", name);
            this.setAttribute("database", database);
            this.setAttribute("table", table);
            this.setAttribute("columns", columns);
            this.setAttribute("indexInfoList", indexInfoList);
            this.setAttribute("action", "alter");
        }
        catch(SQLException e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            Jdbc.close(connection);
        }
        this.forward(page);
    }
}
