/*
 * $RCSfile: ProcessHandler.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-4-24 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.skin.webcat.database.Column;

/**
 * <p>Title: ProcessHandler</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public interface ProcessHandler {
    /**
     * @param columns
     * @param dataSet
     * @throws SQLException
     */
    public void addBatch(List<Column> columns, DataSet dataSet) throws SQLException;

    /**
     * @return int[]
     * @throws SQLException
     */
    public int[] executeBatch() throws SQLException;

    /**
     * @param sql
     * @return int
     * @throws SQLException
     */
    public int delete(String sql) throws SQLException;

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName);

    /**
     * @return the tableName
     */
    public String getTableName();

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection);

    /**
     * @return the connection
     */
    public Connection getConnection();

    /**
     * @return the statement
     */
    public PreparedStatement getStatement();
}
