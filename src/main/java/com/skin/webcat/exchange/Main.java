/*
 * $RCSfile: Main.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-3-27 $
 *
 * Copyright (C) 2008 WanMei, Inc. All rights reserved.
 *
 * This software is the proprietary information of WanMei, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange;

import java.io.File;
import java.sql.Connection;
import java.util.Properties;

import com.skin.webcat.config.Options;
import com.skin.webcat.util.Jdbc;

/**
 * <p>Title: Main</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @version 1.0
 */
public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
        if(args == null || args.length < 2) {
            Main.usage();
            return;
        }

        Options options = new Options(args);
        String arguments = options.getArguments();

        if(arguments.length() > 0) {
            System.out.println("Options: " + arguments);
            System.out.println();
        }

        String url = options.getOption("-url", null);
        String driverClass = options.getOption("-driver", null);
        String userName = options.getOption("-userName", null);
        String password = options.getOption("-password", null);
        String prop = options.getOption("-prop", null);

        if(url == null) {
            System.out.println("url must be not null !");
            Main.usage();
            return;
        }

        if(driverClass == null) {
            System.out.println("driverClass must be not null !");
            Main.usage();
            return;
        }

        Connection connection = null;

        try {
            if(prop != null) {
                Properties properties = Jdbc.parse(prop);
                connection = Jdbc.connect(url, driverClass, properties);
            }
            else {
                connection = Jdbc.connect(url, driverClass, userName, password);
            }

            execute(connection, args);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(connection);
        }
    }

    /**
     * @param connection
     * @param args
     * @throws Exception
     */
    public static void execute(Connection connection, String[] args) throws Exception {
        if(args == null || args.length < 2) {
            Main.usage();
            return;
        }

        Options options = new Options(args);
        String arguments = options.getArguments();

        if(arguments.length() > 0) {
            System.out.println("Options: " + arguments);
            System.out.println();
        }

        String type = options.getOption("-type", null);
        boolean header = options.getBoolean("-header", true);
        String output = options.getOption("-output", ".");

        if(type == null) {
            type = "text";
        }

        DataExport dataExport = null;

        if(type.equals("sql")) {
            dataExport = new SqlDataExport();
        }
        else if(type.equals("csv")) {
            dataExport = new CsvDataExport();
        }
        else {
            dataExport = new DefaultDataExport();
        }

        dataExport.setHeader(header);
        Backup backup = new Backup();
        backup.setConnection(connection);
        backup.setDataExport(dataExport);
        backup.execute(new File(output));
    }

    /**
     * usage
     */
    public static void usage() {
        System.out.println("Usage:");
        System.out.println("    -url    jdbc:mysql://localhost/mydb");
        System.out.println("    -driver com.mysql.jdbc.Driver");
        System.out.println("    -prop   a=1&b=2");
        System.out.println("    -output C:\\backup");
        System.out.println();
    }
}
