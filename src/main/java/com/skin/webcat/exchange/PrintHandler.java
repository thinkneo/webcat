/*
 * $RCSfile: PrintHandler.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-4-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.skin.webcat.database.Column;

/**
 * <p>Title: PrintHandler</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class PrintHandler extends DefaultHandler {
    /**
     * default
     */
    public PrintHandler() {
    }

    /**
     * @param connection
     */
    public PrintHandler(Connection connection) {
        super(connection);
    }

    /**
     * @param columns
     * @param dataSet
     * @return boolean
     * @throws SQLException
     */
    public boolean execute(List<Column> columns, DataSet dataSet) throws SQLException {
        System.out.println(this.getInsertSql(columns, dataSet));
        return true;
    }
}
