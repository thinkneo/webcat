/*
 * $RCSfile: DateTransform.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-3-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange.transform;

import java.text.DateFormat;
import java.util.Date;

/**
 * <p>Title: DateTransform</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class DateTransform extends AbstractTransform {
    private DateFormat dateFormat;

    /**
     * default
     */
    public DateTransform() {
    }

    /**
     * @param quote
     */
    public DateTransform(String quote) {
        super(quote, true);
    }

    /**
     * @param quote
     * @param nullable
     */
    public DateTransform(String quote, boolean nullable) {
        super(quote, nullable);
    }

    /**
     * @param dateFormat
     */
    public DateTransform(DateFormat dateFormat) {
        this.dateFormat = dateFormat;
    }

    /**
     * @param object
     * @return String
     */
    @Override
    public String toString(Object object) {
        if(object != null) {
            Date date = ((Date)object);

            if(this.dateFormat != null) {
                StringBuilder buffer = new StringBuilder();
                buffer.append(this.getQuote());
                buffer.append(this.dateFormat.format(date));
                buffer.append(this.getQuote());
                return buffer.toString();
            }

            return String.valueOf(date.getTime());
        }

        if(this.getNullable()) {
            return "NULL";
        }
        else {
            return this.quote("");
        }
    }
}
