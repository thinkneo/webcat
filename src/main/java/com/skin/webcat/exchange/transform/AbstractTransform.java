/*
 * $RCSfile: AbstractTransform.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-3-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange.transform;

/**
 * <p>Title: AbstractTransform</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class AbstractTransform implements DataTransform {
    private String quote;
    private boolean nullable;

    /**
     * default
     */
    public AbstractTransform() {
        this.quote = "\"";
        this.nullable = true;
    }

    /**
     * @param quote
     */
    public AbstractTransform(String quote) {
        this.quote = quote;
        this.nullable = true;
    }

    /**
     * @param quote
     * @param nullable
     */
    public AbstractTransform(String quote, boolean nullable) {
        this.quote = quote;
        this.nullable = nullable;
    }

    /**
     * @param source
     * @return String
     */
    protected String quote(String source) {
        StringBuilder buffer = new StringBuilder();
        buffer.append(this.quote);

        if(source != null) {
            char c;

            for(int i = 0, size = source.length(); i < size; i++) {
                c = source.charAt(i);

                switch (c) {
                    case '\\': {
                        buffer.append("\\\\"); break;
                    }
                    case '\'': {
                        buffer.append("\\\'"); break;
                    }
                    case '"': {
                        buffer.append("\\\""); break;
                    }
                    case '\r': {
                        buffer.append("\\r"); break;
                    }
                    case '\n': {
                        buffer.append("\\n"); break;
                    }
                    case '\t': {
                        buffer.append("\\t"); break;
                    }
                    case '\b': {
                        buffer.append("\\b"); break;
                    }
                    case '\f': {
                        buffer.append("\\f"); break;
                    }
                    default : {
                        buffer.append(c); break;
                    }
                }   
            }
        }

        buffer.append(this.quote);
        return buffer.toString();
    }

    /**
     * @return the quote
     */
    public String getQuote() {
        return this.quote;
    }

    /**
     * @param quote the quote to set
     */
    public void setQuote(String quote) {
        this.quote = quote;
    }

    /**
     * @param nullable the nullable to set
     */
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    /**
     * @return the nullable
     */
    public boolean getNullable() {
        return this.nullable;
    }

    /**
     * @param object
     * @return String
     */
    @Override
    public String toString(Object object) {
        if(object != null) {
            return object.toString();
        }

        if(this.getNullable()) {
            return "NULL";
        }
        else {
            return this.quote("");
        }
    }
}
