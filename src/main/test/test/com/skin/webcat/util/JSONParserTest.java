/*
 * $RCSfile: JSONParserTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat.util;

import java.util.List;
import java.util.Map;

import com.skin.webcat.util.JSONParser;

/**
 * <p>Title: JSONParserTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * @author xuesong.net
 * @version 1.0
 */
public class JSONParserTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        test("123", Long.valueOf(123));
        test("-123", Long.valueOf(-123));
        test("bbb", null);
        test("null", null);
        test("true", Boolean.TRUE);
        test("false", Boolean.TRUE);
        test("undefined", null);
        test("[1, \r\n2, \r\n\r\n3, {a: 1, b: 2}]", List.class);
        test("{a: 1, b: 2}", Map.class);
        test("{a   /* dsfasfdasfsfdsa\r\ndasfaf\r\n */  : \r\n{c: \r\n1, d: \r\n2, e: '3\\n567\\n33', f: [\r\n1, 2, 3, 4]}, b: \r\n2, c: \r\n   \r\n  true, d: -456}", Map.class);
    }

    /**
     * @param source
     * @param target
     */
    public static void test(String source, Object target) {
        Object value = null;
        System.out.println(source);
        System.out.println("---------------------");

        try {
            value = new JSONParser().parse(source);
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }

        if(target instanceof Class<?>) {
            System.out.println("assert: " + is(value, (Class<?>)target));
        }
        else {
            System.out.println("assert: " + equals(value, target));
        }

        if(value != null) {
            System.out.println(value.getClass().getName() + ": " + value);
        }
        else {
            System.out.println("null");
        }
        System.out.println("=====================");
    }

    /**
     * @param value
     * @param target
     * @return boolean
     */
    public static boolean is(Object value, Class<?> target) {
        return (value != null && target.isAssignableFrom(value.getClass()));
    }

    /**
     * @param obj1
     * @param obj2
     * @return boolean
     */
    public static boolean equals(Object obj1, Object obj2) {
        if(obj1 != null) {
            return obj1.equals(obj2);
        }

        if(obj2 != null) {
            return obj2.equals(obj1);
        }
        return true;
    }
}
