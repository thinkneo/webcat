/*
 * $RCSfile: ConnectTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.sql.Connection;
import java.util.List;

import com.skin.webcat.database.handler.TableHandler;
import com.skin.webcat.util.Jdbc;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: ConnectTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ConnectTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        Connection connection = null;

        try {
            connection = Webcat.getConnection("localhost4");
            TableHandler tableHandler = new TableHandler(connection);
            List<String> list = tableHandler.getDatabase(connection);

            for(String database : list) {
                System.out.println(database);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(connection);
        }
    }
}
