/*
 * $RCSfile: InsertParserTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat.database.sql;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.skin.webcat.database.Record;
import com.skin.webcat.database.sql.parser.InsertParser;
import com.skin.webcat.util.IO;

/**
 * <p>Title: InsertParserTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * @author xuesong.net
 * @version 1.0
 */
public class InsertParserTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            String source = IO.read(new File("D:\\workspace\\jartest\\test1.sql"), "utf-8");
            InsertParser parser = new InsertParser();
            List<Record> resultSet = parser.parse(source);

            for(Record record : resultSet) {
                System.out.println(record.toString());
            }
            // write(new File("test2.sql"), resultSet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     */
    public static void test() {
        StringBuilder sql = new StringBuilder();
        sql.append("insert into a(`a1`, [a2], 'a3') values ('a', 1, 1)\r\n");
        sql.append("insert into a(`a1`, a2, a3) values ('b', 2, 2);\r\n");
        sql.append("insert into a(`a1`, a2, a3) values ('c', null, 3)");

        try {
            InsertParser parser = new InsertParser();
            List<Record> resultSet = parser.parse(sql.toString());

            for(Record record : resultSet) {
                System.out.println(record.toString());
            }
            parser.print(resultSet, "insert into mytable(`b1`, `b2`, `b3`) values (${a1}, ${a2}, ${a3});");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
