/*
 * $RCSfile: MySqlTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2016-11-5 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.skin.database.Jdbc;
import com.skin.webcat.database.mysql.MySql;
import com.skin.webcat.database.mysql.TableIndex;
import com.skin.webcat.util.Sql;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: MySqlTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class MySqlTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        // test1();
        System.out.println(Sql.isSqlIdentifier("idx_test1"));
    }

    /**
     * test1
     */
    public static void test1() { 
        Connection connection = null;
        String name = "localhost1";
        String database = "baike";

        try {
            connection = Webcat.getConnection(name, database);
            List<TableIndex> tableIndexList = MySql.getTableIndexList(connection, "bk_archive2", true, false);
            System.out.println(JSON.toJSONString(tableIndexList));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(connection);
        }
    }
}
