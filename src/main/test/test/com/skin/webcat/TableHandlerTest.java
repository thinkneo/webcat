/*
 * $RCSfile: TableHandlerTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-04-09 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.skin.database.Jdbc;
import com.skin.webcat.database.Column;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.handler.TableHandler;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: TableHandlerTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableHandlerTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Connection connection = null;
        String name = "localhost1";
        String database = "baike";
        String tableName = "bk_archive2";

        try {
            connection = Webcat.getConnection(name, database);
            TableHandler tableHandler = new TableHandler(connection);
            Table table = tableHandler.getTable(tableName);
            List<Column> columns = table.getColumns();

            if(columns != null) {
                for(Column column : columns) {
                    column.setTable(null);
                }
            }
            System.out.println(JSON.toJSONString(table, true));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(connection);
        }
    }
}
