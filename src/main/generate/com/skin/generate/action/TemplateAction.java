/*
 * $RCSfile: TemplateAction.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-4-2 $
 *
 * Copyright (C) 2008 WanMei, Inc. All rights reserved.
 *
 * This software is the proprietary information of WanMei, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.action;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.generate.Template;
import com.skin.generate.TemplateConfig;
import com.skin.generate.TemplateParser;
import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;

/**
 * <p>Title: TemplateAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author chenyankui
 * @version 1.0
 */
public class TemplateAction extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(TemplateAction.class);

    /**
     * @throws IOException 
     * @throws ServletException 
     */
    @UrlPattern("/generate/template.html")
    public void execute() throws IOException, ServletException {
        String config = this.getServletContext().getRealPath("/config");
        String templateConfig = this.getTrimString("templateConfig");
        List<String> templateConfigList = TemplateConfig.getTemplateConfigList(config);

        if(templateConfig.length() < 1) {
            if(templateConfigList != null && templateConfigList.size() > 0) {
                templateConfig = templateConfigList.get(0);
            }
        }

        if(templateConfig.length() < 1) {
            throw new ServletException("缺少模板配置！");
        }

        File file = new File(config, templateConfig);

        try {
            List<Template> templateList = TemplateParser.parseTemplates(file);
            this.setAttribute("templateList", templateList);
            this.setAttribute("templateConfig", templateConfig);
            this.setAttribute("templateConfigList", templateConfigList);
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
        this.forward("/template/generate/template.jsp");
    }
}
