/*
 * $RCSfile: TableAction.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-3-26 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.generate.action;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.generate.Template;
import com.skin.generate.TemplateConfig;
import com.skin.generate.TemplateParser;
import com.skin.j2ee.action.BaseAction;
import com.skin.j2ee.annotation.UrlPattern;
import com.skin.webcat.database.Column;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.TableType;
import com.skin.webcat.database.handler.TableHandler;
import com.skin.webcat.util.Jdbc;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: TableAction</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableAction extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(TableAction.class);

    /**
     * @throws IOException 
     * @throws ServletException 
     */
    @UrlPattern("/generate/table/list.html")
    public void list() throws IOException, ServletException {
        String templateConfig = this.request.getParameter("templateConfig");
        String connectionName = this.request.getParameter("connectionName");
        String[] types = TableType.getTypes(this.request.getParameterValues("type"));

        if(types.length < 1) {
            this.error(500, "\"type\" must be not null !");
            return;
        }

        if(templateConfig != null) {
            templateConfig = templateConfig.trim();
        }

        Connection connection = null;

        try {
            connection = Webcat.getConnection(connectionName);
            TableHandler tableHandler = new TableHandler(connection);
            List<Table> tableList = tableHandler.getTableList("%", types, true);
            this.request.setAttribute("templateConfig", templateConfig);
            this.request.setAttribute("connectionName", connectionName);
            this.request.setAttribute("tableList", tableList);
        }
        catch(SQLException e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            Jdbc.close(connection);
        }
        this.forward("/template/generate/tableList.jsp");
    }

    /**
     * @throws IOException
     * @throws ServletException
     */
    @UrlPattern("/generate/table/edit.html")
    public void edit() throws IOException, ServletException {
        String connectionName = this.request.getParameter("connectionName");
        String database = this.request.getParameter("database");
        String tableName = this.request.getParameter("tableName");
        String templateConfig = this.request.getParameter("templateConfig");
        Connection connection = null;

        if(templateConfig == null || templateConfig.trim().length() < 1) {
            templateConfig = "template.default.xml";
        }

        try {
            connection = Webcat.getConnection(connectionName, database);
            String file = this.getServletContext().getRealPath("/config/" + templateConfig);
            TableHandler tableHandler = new TableHandler(connection);
            Table table = tableHandler.getTable(tableName);
            List<Column> columns = table.getColumns();
            List<Template> templates = TemplateParser.parseTemplates(new File(file));
            List<String> templateConfigList = TemplateConfig.getTemplateConfigList(this.getServletContext().getRealPath("/config"));

            if(columns != null) {
                for(Column column : columns) {
                    column.setTable(null);
                }
            }

            this.request.setAttribute("table", table);
            this.request.setAttribute("columns", columns);
            this.request.setAttribute("templates", templates);
            this.request.setAttribute("connectionName", connectionName);
            this.request.setAttribute("tableName", tableName);
            this.request.setAttribute("templateConfig", templateConfig);
            this.request.setAttribute("templateConfigList", templateConfigList);
            this.request.setAttribute("author", System.getProperty("user.name"));
        }
        catch(SQLException e) {
            logger.error(e.getMessage(), e);
        }
        finally {
            Jdbc.close(connection);
        }
        this.forward("/template/generate/tableEdit.jsp");
    }
}
